import os
from sys import stderr

from flask import Flask, render_template, request, url_for, redirect
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from dotenv import load_dotenv



app = Flask(__name__)

load_dotenv()
app.config["MONGO_URI"] = os.getenv("MONGO_URI")
print(os.getenv("MONGO_URI"), file=stderr)
mongo = PyMongo(app)

leagueofstat = mongo.db.leagueofstat

@app.route('/', methods=['GET', 'POST'])
def index():
    classe = leagueofstat.find().distinct('classe')
    poste = leagueofstat.find().distinct('poste')
    if request.method == "POST":
        selected_classe = request.form.getlist("classe")
        selected_poste = request.form.getlist("poste")
        recherche = request.form.get("recherche")
        if recherche:
            recherche = str.capitalize(recherche)
            leagueofstats = leagueofstat.find({"nom": {"$regex": recherche}})
        elif not selected_poste and not selected_classe and not recherche:
            leagueofstats = leagueofstat.find().sort("nom", 1)
        elif not selected_poste:
            leagueofstats = leagueofstat.find(
                {"classe": {"$in": selected_classe}}
            ).sort("nom", 1)
        elif not selected_classe:
            leagueofstats = leagueofstat.find(
                {"poste": {"$in": selected_poste}}
            ).sort("nom", 1)
        else:
            leagueofstats = leagueofstat.find(
                {"$and": [
                    {"classe": {"$in": selected_classe}},
                    {"poste": {"$in": selected_poste}},
                ]}
            ).sort("nom", 1)
        countChampion = leagueofstats.count()
        return render_template('index.html', leagueofstats=leagueofstats, classe=classe, poste=poste, countChampion=countChampion)
    else:
        leagueofstats = leagueofstat.find().sort("nom", 1)
        countChampion = leagueofstats.count()
        return render_template('index.html', leagueofstats=leagueofstats, classe=classe, poste=poste, countChampion=countChampion)


@app.route('/detail/<id>', methods=['GET'])
def detail(id):
    solo_leagueofstat = leagueofstat.find_one({'_id': ObjectId(id)})
    return render_template('detail.html', solo_leagueofstat=solo_leagueofstat)


@app.route('/add_champion', methods=['GET'])
def page_add_champion():
    return render_template('add_champion.html')

@app.route('/add', methods=['POST'])
def add_champion():
    nom = request.form.get('nom')
    surnom = request.form.get('surnom')
    classe = request.form.get('classe')
    poste = request.form.get('poste')
    description = request.form.get('description')
    competences = request.form.getlist('competences[]')
    skin = request.form.getlist('skin[]')
    eb = request.form.get('eb')
    rp = request.form.get('rp')
    image_card = request.form.get('image_card')
    image_back = request.form.get('image_back')
    leagueofstat.insert_one({'nom': nom, 'surnom': surnom, 'classe': classe, 'poste': poste, 'description': description, 'competences': competences, 'skin': skin, 'eb': eb, 'rp': rp, 'image_card': image_card, 'image_back': image_back})
    return redirect(url_for('index'))


@app.route('/leagueofstat_delete/<id>', methods=['POST'])
def delete_champion(id):
    leagueofstat.delete_one({'_id': ObjectId(id)})
    return redirect(url_for('index'))


@app.route('/leagueofstat/<id>/add_skin', methods=['POST'])
def add_skin_champion(id):
    solo_leagueofstat = leagueofstat.find_one({'_id': ObjectId(id)})
    solo_leagueofstat['skin'].append(request.form.get('skin[]'))
    leagueofstat.replace_one({'_id': ObjectId(id)}, solo_leagueofstat)
    return redirect(url_for('detail', id=id))

@app.route('/leagueofstat/<id>/delete_skin', methods=['POST'])
def delete_skin_champion(id):
    skin = request.form.getlist('skin[]')
    leagueofstat.delete_one({'skin': skin})
    return redirect(url_for('detail', id=id))



@app.route('/leagueofstat_update/<id>', methods=['POST'])
def update_champion(id):
    nom = request.form.get('nom')
    surnom = request.form.get('surnom')
    classe = request.form.get('classe')
    poste = request.form.get('poste')
    description = request.form.get('description')
    competences = request.form.getlist('competences[]')
    skin = request.form.getlist('skin[]')
    eb = request.form.get('eb')
    rp = request.form.get('rp')
    image_card = request.form.get('image_card')
    image_back = request.form.get('image_back')

    updated_leagueofstat = leagueofstat.find_one({'_id': ObjectId(id)})

    updated_leagueofstat["nom"] = nom
    updated_leagueofstat["surnom"] = surnom
    updated_leagueofstat["classe"] = classe
    updated_leagueofstat["poste"] = poste
    updated_leagueofstat["description"] = description
    updated_leagueofstat["competences"] = competences
    updated_leagueofstat["skin"] = skin
    updated_leagueofstat["eb"] = eb
    updated_leagueofstat["rp"] = rp
    updated_leagueofstat["image_card"] = image_card
    updated_leagueofstat["image_back"] = image_back

    leagueofstat.replace_one({'_id': ObjectId(id)}, updated_leagueofstat)
    return redirect(url_for('detail', id=id))


@app.route('/testmoche', methods=['GET'])
def index_test():
    leagueofstats = leagueofstat.find()
    return render_template('index_test.html', leagueofstats=leagueofstats)


@app.route('/leagueofstat/<id>', methods=['GET'])
def detail_champion_test(id):
    solo_leagueofstat = leagueofstat.find_one({'_id': ObjectId(id)})
    return render_template('detail_test.html', solo_leagueofstat=solo_leagueofstat)

@app.route('/style/<classe>', methods=['GET'])
def classe_champion(classe):
    classeFiltre = leagueofstat.find().distinct('classe')
    poste = leagueofstat.find().distinct('poste')
    leagueofstats = leagueofstat.find({'classe': classe}).sort("nom", 1)
    countChampion = leagueofstats.count()
    return render_template("index.html", leagueofstats=leagueofstats, classe=classeFiltre, poste=poste, countChampion=countChampion)

@app.route('/style/<classe>/desc', methods=['GET'])
def classe_champion_desc(classe):
    classeFiltre = leagueofstat.find().distinct('classe')
    poste = leagueofstat.find().distinct('poste')
    leagueofstats = leagueofstat.find({'classe': classe}).sort("nom", -1)
    countChampion = leagueofstats.count()
    return render_template("index.html", leagueofstats=leagueofstats, classe=classeFiltre, poste=poste, countChampion=countChampion)

@app.route('/poste/<poste>', methods=['GET'])
def poste_champion(poste):
    classeFiltre = leagueofstat.find().distinct('classe')
    posteFiltre = leagueofstat.find().distinct('poste')
    leagueofstats = leagueofstat.find({'poste': poste}).sort("nom", 1)
    countChampion = leagueofstats.count()
    return render_template("index.html", leagueofstats=leagueofstats, classe=classeFiltre, poste=posteFiltre, countChampion=countChampion)

@app.route('/desc/', methods=['GET', 'POST'])
def index_desc():
    classe = leagueofstat.find().distinct('classe')
    poste = leagueofstat.find().distinct('poste')
    leagueofstats = leagueofstat.find().sort("nom", -1)
    countChampion = leagueofstats.count()
    return render_template('index.html', leagueofstats=leagueofstats, classe=classe, poste=poste,countChampion=countChampion)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
