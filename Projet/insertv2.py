from run import mongo

champion = {
    "nom": "Ahri",
    "surnom": "RENARD À NEUF QUEUES",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Dotée d'un lien inné avec le pouvoir naturel de Runeterra, Ahri est une Vastaya capable de modeler la magie pour en faire des orbes d'énergie pure. Elle aime plus que tout jouer avec ses proies en manipulant leurs émotions avant de dévorer leur essence vitale. En dépit de sa nature de prédateur, Ahri n'est pas sans empathie, car chaque âme qu'elle absorbe l'emplit de fragments de ses souvenirs.",
    "competences": ["VOL ESSENTIEL", "ORBE D'ILLUSION", "LUCIOLES", "CHARME", "ASSAUT SPIRITUEL"],
    "skin": ["Ahri", "Ahri Dynastique", "Ahri De Minuit", "Ahri Renard du Feu", "Ahri Popstar", "Ahri Challenger", "Ahrithmétique", "Ahri Arcade", "Ahri Gardienne des Etoiles", "K/DA Ahri", "K/DA Ahri Edition Prestige", "Ahri Sylvestre", "Ahri Fleur Spirituelle", "K/DA ALL OUT Ahri"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt1259276b6d1efa78/5db05fa86e8b0c6d038c5ca2/RiotX_ChampionList_ahri.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ahri_0.jpg",

    "eb": 4800,
    "rp": 880

},{
    "nom": "Akali",
    "surnom": "ASSASSIN REBELLE",
    "classe": "Assassin",
    "poste": "Milieu / Haut",
    "description": "Ayant abandonné l'Ordre Kinkou et le titre de Poing de l'ombre, Akali combat aujourd'hui seule, prête à devenir l'arme mortelle dont son peuple a besoin. Bien qu'elle n'oublie rien de tout ce que son maître Shen lui a enseigné, elle a juré de défendre Ionia contre ses ennemis, une élimination après l'autre. Akali tue sans faire de bruit, mais son message est fort et clair : craignez l'assassin sans maître.",
    "competences": ["MARQUE D'ASSASSIN", "VAGUE DE KUNAIS", "LINCEUL NÉBULEUX", "LANCER ACROBATIQUE", "MAÎTRISE ABSOLUE"],
    "skin": ["Akali", "Akali Aiguillon", "Akali Infernale", "Akali ALL-STAR", "Akali Infirmière", "Akali Lune de Sang", "Akali Crocs D'argent", "Akali Chasseuse de Têtes", "Akali Chef Sushi", "K/DA Akali", "K/DA Akali Edition Prestige", "PROJET : Akali", "True Damage Akali", "K/DA All Out Akali"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt80ff58fe809777ff/5db05fa8adc8656c7d24e7d6/RiotX_ChampionList_akali.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Akali_0.jpg",

    "eb": 3150,
    "rp": 790
},{
    "nom": "Anivia",
    "surnom": "CRYOPHENIX",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Anivia est un esprit ailé bienveillant qui subit des cycles sans fin de vie, de mort et de renaissance pour protéger Freljord. Demi-déesse née de la glace la plus froide et des vents les plus coupants, elle utilise ses pouvoirs élémentaires contre quiconque prétend troubler la paix de sa terre natale. Anivia guide et protège les tribus du Nord, qui la vénèrent comme un symbole d'espoir et la promesse de grands changements. Elle combat de tout son être, sachant que son sacrifice fera vivre sa légende et qu'elle renaîtra dans un lendemain nouveau.",
    "competences": ["RENAISSANCE", "LANCE DE GLACE", "CRISTALLISATION", "GELURE", "TEMPÊTE GLACIALE"],
    "skin": ["Anivia", "Anivia Esprit D'équipe", "Anivia Oiseau de Proie", "Anivia Chasseresse de Noxus", "Anivia Hextech", "Anivia des Glaces Noires", "Anivia Préhistorique", "Anivia Reine du Carnaval", "Anivia Origami", "Anivia Oiseau Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3d24a1482453088a/5db05fa8df78486c826dcce8/RiotX_ChampionList_anivia.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Anivia_0.jpg",

    "eb": 3150,
    "rp": 790
},{
    "nom": "Annie",
    "surnom": "ENFANT DES TÉNÈBRES",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Dangereuse, incroyablement précoce, Annie est une enfant mage dotée d'extraordinaires pouvoirs de pyrokinésie. Même à l'ombre des montagnes du nord de Noxus, sa magie est un cas unique. Son affinité naturelle avec le feu se manifesta dès sa prime enfance à travers des explosions émotionnelles imprévisibles, même si elle apprit rapidement à contrôler ces éclats. Parmi ses jeux favoris, elle aime invoquer son bien-aimé ours en peluche, Tibbers, sous la forme d'un protecteur enflammé. Perdue dans l'innocence perpétuelle de l'enfance, Annie sautille dans les forêts obscures, à la recherche de quelqu'un avec qui jouer.",
    "competences": ["PYROMANIE", "DÉSINTÉGRATION", "INCINÉRATION", "BOUCLIER EN FUSION", "INVOCATION : TIBBERS"],
    "skin": ["Annie", "Annie Gothique", "Annie Chaperon Rouge", "Annie au Pays des Merveilles", "Annie Reine du Bal", "Annie Polaire", "Annie Nounours", "Annie Frankentibbers", "Annie Panda", "Annie Coeur Tendre", "Annie Hextech", "Super Annie Intergalactique", "Annie-Versaire", "Annie Bête Lunaire"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt28c708665427aef6/5db05fa89481396d6bdd01a6/RiotX_ChampionList_annie.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Annie_0.jpg",

    "eb": 450,
    "rp": 260
},{
    "nom": "Aphelios",
    "surnom": "ARME DES LUNARIS",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Émergeant des ombres au clair de lune, Aphelios abat ceux qui voudraient anéantir sa foi sans un mot ; ses armes et sa précision mortelle parlent pour lui. Un poison qui le rend muet coule dans ses veines, mais il est constamment guidé par sa sœur, Alune. Depuis son temple lointain, elle lui confère un arsenal d'armes en pierre de lune. Tant que la lune brillera dans le ciel, Aphelios ne sera jamais seul",
    "competences": ["LE TUEUR ET L'ORACLE", "COMPÉTENCES D'ARME", "PHASE", "SYSTÈME DE FILE D'ARMES", "VEILLE AU CLAIR DE LUNE"],
    "skin": ["Aphelios", "Aphelios Héraut de la Nuit", "Aphelios Bête Lunaire"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Aphelios_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Aphelios_0.jpg",

    "eb": 6300,
    "rp": 975
},{
    "nom": "Ashe",
    "surnom": "ARCHÈRE DE GIVRE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Chef de guerre sublimé de la tribu des Avarosans, Ashe est à la tête de la plus vaste horde des terres du nord. Stoïque, intelligente et idéaliste, mais mal à l'aise dans son rôle de leader, elle puise dans la magie ancestrale de sa lignée pour manier un arc de glace pure. Soutenue par son peuple qui voit en elle la réincarnation de la légendaire héroïne Avarosa, Ashe cherche à unifier Freljord en reprenant les terres tribales.",
    "competences": ["TIR GIVRANT", "CONCENTRATION DU RANGER", "SALVE", "RAPACE", "FLÈCHE DE CRISTAL ENCHANTÉE"],
    "skin": ["Ashe", "Ashe de Freljord", "Ashe de Sherwood", "Ashe Pastel", "Reine Ashe", "Ashe D'Améthyste", "Ashe Coeur de Cible", "Ashe Maraudeuse", "Ashe du Championnat", "Ashe Reine Cosmique", "Ashe de L'Ouest", "Ashe Dragon Féérique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt943aae038e2dee98/5db05fa8e9effa6ba5295f91/RiotX_ChampionList_ashe.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ashe_0.jpg",

    "eb": 450,
    "rp": 260
},{
    "nom": "Aurelion Sol",
    "surnom": "FORGEUR D'ÉTOILES",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Autrefois, Aurelion Sol façonnait des merveilles célestes dont il parsemait le vide infini du cosmos. À présent, il est forcé d'utiliser ses pouvoirs extraordinaires pour le compte d'un empire de l'espace qui s'est joué de lui et l'a réduit en esclavage. Prêt à tout pour redevenir le forgeron stellaire qu'il était, Aurelion Sol n'hésitera pas à arracher les étoiles de leur ciel nocturne si c'est là le prix à payer pour regagner sa liberté.",
    "competences": ["CENTRE DE L'UNIVERS", "STELLOGENÈSE", "EXPANSION CÉLESTE", "COMÈTE LÉGENDAIRE", "CRI DE LUMIÈRE"],
    "skin": ["Aurelion Sol", "Aurelion Sol Seigneur Volcanique", "Mecha Aurelion Sol", "Aurelion Sol Dragon des Tempêtes"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5dd3569fc67d6664/5db05fa8bd24496c390ae4d8/RiotX_ChampionList_aurelionsol.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/AurelionSol_0.jpg",

    "eb": 6300,
    "rp": 975
},{
    "nom": "Azir",
    "surnom": "EMPEREUR DES SABLES",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Azir fut l'empereur mortel de Shurima en des temps très lointains, un homme fier dressé au bord de l'immortalité. Son orgueil lui valut d'être trahi et assassiné à l'heure de son triomphe ; mais des millénaires se sont écoulés et il revient sous la forme d'un être transfiguré à l'immense puissance. Sa cité ensevelie ayant ressurgi des sables, Azir cherche à rendre sa gloire passée à Shurima.",
    "competences": ["HÉRITAGE DE SHURIMA", "SABLES CONQUÉRANTS", "DRESSE-TOI !", "SABLES MOUVANTS", "PARTITION IMPÉRIALE"],
    "skin": ["Azir", "Azir Galactique", "Azir Nécromancien", "SKT T1 Azir", "Azir du Royaume en Guerre", "Azir Sylvestre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt0e3f847946232167/5db05fa889fb926b491ed7ff/RiotX_ChampionList_azir.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Azir_0.jpg",

    "eb": 6300,
    "rp": 975
},{
    "nom": "Bard",
    "surnom": "GARDIEN ERRANT",
    "classe": "Support",
    "poste": "Support",
    "description": "Voyageur d'au-delà des étoiles, Bard est un messager des bons augures qui combat pour maintenir l'équilibre dont la vie a besoin pour prospérer dans l'indifférence du chaos. Dans tout Runeterra, sa mystérieuse nature inspire des chants qui ne sont d'accord que sur un point : le vagabond cosmique est attiré par les reliques dotées d'un grand pouvoir magique. Accompagné par un chœur joyeux de Meeps, des esprits qui lui sont dévoués, il n'a d'actions que bénéfiques, même s'il a sa propre manière, un peu étrange, de servir le bien.",
    "competences": ["INSTINCT DU VOYAGEUR", "LIEN COSMIQUE", "DON DU GARDIEN", "ROUTE MAGIQUE", "DESTIN TEMPÉRÉ"],
    "skin": ["Bard", "Bard Sylvestre", "Bard des Neiges", "Bard le Barde", "Bard Astronaute"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltbbe3ce0c0ae1305b/5db05fb23a326d6df6c0e7b3/RiotX_ChampionList_bard.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Bard_0.jpg",

    "eb": 6300,
    "rp": 975
},{
    "nom": "Brand",
    "surnom": "VENGEUR FLAMBOYANT",
    "classe": "Mage / Support",
    "poste": "Milieu / Support",
    "description": "Autrefois membre d'une tribu de Freljord, Kegan Rodhe est devenu l'être que l'on connaît sous le nom de Brand à force de convoiter des pouvoirs toujours plus grands. Alors qu'il recherchait l'une des légendaires Runes telluriques, Kegan trahit ses compagnons et s'en empara ; en un instant, son âme fut consumée par un brasier qui le changea à jamais. Désormais animé d'un feu inextinguible, Brand parcourt Valoran en quête d'autres runes, jurant vengeance pour des trahisons qu'aucun mortel n'aurait pu subir au cours d'une dizaine de vies.",
    "competences": ["FLAMMES", "BRÛLURE", "COLONNE DE FLAMMES", "CONFLAGRATION", "PYROLYSE"],
    "skin": ["Brand", "Brand Apocalyptique", "Brand Vandale", "Cryo Brand", "Brand Zombie", "Brand Boss de Combat", "Brand Aux Flammes Pures", "Brand Dragon Eternel"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc8ca2e9bba653dda/5db05fb2dc674266df3d5d30/RiotX_ChampionList_brand.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Brand_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Caitlyn",
    "surnom": "SHÉRIF DE PILTOVER",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Caitlyn est la plus célèbre gardienne de la paix à Piltover, mais elle est aussi la plus apte à débarrasser la ville de ses criminels les plus insaisissables. Elle fait souvent équipe avec Vi, son calme faisant contrepoids à la fougue de sa partenaire. Bien qu'elle porte un fusil Hextech unique en son genre, la meilleure arme de Caitlyn reste son intelligence ; elle invente en effet des pièges élaborés pour attraper les bandits qui auraient l'audace d'agir dans la Cité du progrès.",
    "competences": ["TIR DANS LA TÊTE", "PACIFICATEUR DE PILTOVER", "PIÈGE-YORDLE", "FILET DE CALIBRE 90", "TIR CHIRURGICAL"],
    "skin": ["Caitlyn", "Caitlyn Résistante", "Sherif Caitlyn", "Caitlyn Safari", "Caitlyn Arctique", "Agent Caitlyn", "Caitlyn Chasseuse de Têtes", "Caitlyn Spectre Lunaire", "Caitlyn Pulsefire", "Caitlyn Fusil à Eau", "Caitlyn Arcade", "Caitlyn Arcade Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt014f4b6fc9bacd10/5db05fb00b39e86c2f83dbff/RiotX_ChampionList_caitlyn.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Caitlyn_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Caitlyn",
    "surnom": "SHÉRIF DE PILTOVER",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Caitlyn est la plus célèbre gardienne de la paix à Piltover, mais elle est aussi la plus apte à débarrasser la ville de ses criminels les plus insaisissables. Elle fait souvent équipe avec Vi, son calme faisant contrepoids à la fougue de sa partenaire. Bien qu'elle porte un fusil Hextech unique en son genre, la meilleure arme de Caitlyn reste son intelligence ; elle invente en effet des pièges élaborés pour attraper les bandits qui auraient l'audace d'agir dans la Cité du progrès.",
    "competences": ["TIR DANS LA TÊTE", "PACIFICATEUR DE PILTOVER", "PIÈGE-YORDLE", "FILET DE CALIBRE 90", "TIR CHIRURGICAL"],
    "skin": ["Caitlyn", "Caitlyn Résistante", "Sherif Caitlyn", "Caitlyn Safari", "Caitlyn Arctique", "Agent Caitlyn", "Caitlyn Chasseuse de Têtes", "Caitlyn Spectre Lunaire", "Caitlyn Pulsefire", "Caitlyn Fusil à Eau", "Caitlyn Arcade", "Caitlyn Arcade Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt014f4b6fc9bacd10/5db05fb00b39e86c2f83dbff/RiotX_ChampionList_caitlyn.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Caitlyn_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Cassiopeia",
    "surnom": "ÉTREINTE DU SERPENT",
    "classe": "mage",
    "poste": "Milieu / Haut",
    "description": "Cassiopeia est une créature meurtrière qui excelle dans l'art de la manipulation. Elle était la plus jeune et la plus belle des filles de la famille noxienne Du Couteau, mais le jour où elle s'aventura dans les cryptes de Shurima en quête d'un pouvoir antique, le venin d'un gardien la transforma en un prédateur reptilien. Aussi rusée qu'agile, Cassiopeia se faufile désormais dans les ombres de la nuit, pétrifiant ses ennemis d'un simple regard.",
    "competences": ["GRÂCE SERPENTINE", "BOMBE NOCIVE", "MIASMES", "MORSURE FATALE", "REGARD DE LA MÉDUSE"],
    "skin": ["Cassiopeia", "Cassiopeia Desperada", "Cassiopeia Sirène", "Cassiopeia Mythologique", "Cassiopeia Crochets de Jade", "Cassiopeia Eternum", "Cassiopeia Fleur Spirituelle"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte189be8189da97ea/5db05fb1bd24496c390ae4de/RiotX_ChampionList_cassiopeia.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Cassiopeia_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Corki",
    "surnom": "ARTILLEUR TÉMÉRAIRE",
    "classe": "ADC",
    "poste": "Milieu",
    "description": "Il y a deux choses que Corki, le pilote yordle, aime plus que tout au monde : les sensations de vol et sa fantastique moustache... mais pas nécessairement dans cet ordre. Après avoir quitté Bandle, il s'installa à Piltover, où il tomba amoureux des merveilleuses machines qu'il y trouva. Il consacra tout son temps au développement d'engins volants, tout en menant une unité de défense aérienne composée de pilotes aguerris : les Serpents volants. Serein même sous le feu ennemi, Corki patrouille dans l'espace aérien de sa patrie d'adoption, et il ne rencontre jamais le moindre problème qu'un bon gros missile ne puisse résoudre.",
    "competences": ["MUNITIONS HEXTECH", "BOMBE AU PHOSPHORE", "VALKYRIE", "GATLING", "BARRAGE DE PROJECTILES"],
    "skin": ["Corki", "Corki Ovni", "Corki Toboggan", "Corki Le Baron Rouge", "Corki Hot Rod", "Corki Urfrider", "Corki Ailes du Dragon", "Fnatic Corki", "Corki Arcade", "Corki Corgi"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltdd918c4d0a86347a/5db05fb1df78486c826dccee/RiotX_ChampionList_corki.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Corki_0.jpg",

    "eb": 3150,
    "rp": 790
},{
    "nom": "Diana",
    "surnom": "MÉPRIS DE LA LUNE",
    "classe": "Combattant",
    "poste": "Milieu / Jungle",
    "description": "Armée de son croissant de lune, Diana combat dans les rangs des Lunaris, une foi qui a presque disparu des terres qui entourent le Mont Targon. Portant une armure étincelante évoquant les reflets de la nuit sur un paysage de neige, elle incarne la puissance de la lune. L'essence d'une Manifestation venue d'au-delà des plus hauts sommets de Targon imprègne Diana : plus tout à fait humaine, elle lutte pour comprendre sa puissance et son rôle en ce monde.",
    "competences": ["LAME SÉLÈNE", "CROISSANT LUNAIRE", "CORPS CÉLESTES", "RUSH LUNAIRE", "ATTRACTION LUNAIRE"],
    "skin": ["Diana", "Diana Valkyrie Sombre", "Diana Déesse Lunaire", "Diana Infernale", "Diana Lune de Sang", "Diana des Eaux Sombres", "Diana Tueuse de Dragons", "Diana Reine du Combat", "Diana Reine du Combat Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt56570083d2a5e20e/5db05fbc823d426762825fdf/RiotX_ChampionList_diana.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Diana_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Draven",
    "surnom": "GLORIEUX EXÉCUTEUR",
    "classe": "ADC",
    "poste": "Bas",
    "description": "À Noxus, de puissants guerriers, les « arénaires », s'affrontent dans des jeux mortels pour divertir le public. Le sang y coule à foison et leurs forces y sont éprouvées, mais aucun d'entre eux n'a jamais été aussi célèbre que Draven. Cet ancien soldat découvrit que le public appréciait particulièrement son sens du spectacle, sans parler de son habileté avec des haches tournoyantes. Épris de sa propre perfection, Draven terrasse tous ses adversaires pour s'assurer que son nom continue de résonner dans les arènes de l'empire.",
    "competences": ["LEAGUE OF DRAVEN", "HACHE TOURNOYANTE", "PULSION SANGUINAIRE", "DIVISION", "VOLÉE MORTELLE"],
    "skin": ["Draven", "Draven Faucheur", "Draven Gladiateur", "Draven Prime Time", "Draven Glorieux Nageur", "Draven Chasseur de Bêtes", "Draven Draven", "Père Draven", "Draven Du Royaume des Mechas", "Draven Déchu"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc0be728e4cbb8f2a/5db05fbc89fb926b491ed80b/RiotX_ChampionList_draven.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Draven_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Elise",
    "surnom": "REINE ARAIGNÉE",
    "classe": "Mage",
    "poste": "Jungle",
    "description": "Elise est un prédateur mortel qui vit dans un palais reclus et obscur, au plus profond de la plus vieille ville de Noxus. C'était autrefois une simple femme, maîtresse d'une Maison toute-puissante, mais la morsure d'un maléfique demi-dieu la transforma en un être aussi magnifique qu'inhumain : une créature arachnéenne qui tisse sa toile pour piéger ses proies. Pour maintenir sa jeunesse éternelle, Elise se repaît des innocents et peu sont ceux qui peuvent résister à sa séduction.",
    "competences": ["REINE ARAIGNÉE", "NEUROTOXINE/MORSURE VENIMEUSE", "ARAIGNÉE EXPLOSIVE/FRÉNÉSIE SYMBIOTIQUE", "COCON/SUSPENSION", "FORME ARACHNÉENNE"],
    "skin": ["Elise", "Elise du Lotus Mortel", "Elise Héroïne de Guerre", "Elise Lune de Sang", "SKT T1 Elise", "Super Elise Intergalactique", "Elise Sorcière"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltfe21ff2ddb84d5d1/5db05fbd0b39e86c2f83dc05/RiotX_ChampionList_elise.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Elise_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Ezreal",
    "surnom": "EXPLORATEUR PRODIGUE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Aventurier audacieux, doué sans le savoir pour les arts magiques, Ezreal poursuit ses expéditions dans les catacombes oubliées, se bat avec d'antiques malédictions et contourne avec aisance les obstacles les plus impossibles. Son courage et son panache sont sans borne, et il préfère improviser son salut dans les situations critiques, avec pour armes principales sa vivacité d'esprit et son gantelet mystique shurimien, qu'il utilise pour déchaîner de puissants projectiles arcaniques. Une chose est sûre : quand Ezreal est dans les parages, les problèmes ne sont pas loin. Et ils sont partout. Absolument partout.",
    "competences": ["FORCE GRANDISSANTE", "TIR MYSTIQUE", "FLUX ESSENTIEL", "TRANSFERT ARCANIQUE", "ARC TÉRÉBRANT"],
    "skin": ["Ezreal", "Ezreal de Nottingham", "Ezreal Buteur", "Ezreal de Glace", "Ezreal Spéléologue", "Ezreal PulseFire", "TPA Ezreal", "Ezreal Séducteur", "Ezreal As de Pique", "Ezreal Gardien des Etoiles", "SSG Ezreal", "Ezreal Gardien des Pyjamas", "Ezreal de l'Académie du Combat", "Ezreal Psychoguerrier", "Ezreal Psychoguerrier Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt60f687c95425f73f/5db05fbd2dc72966da746704/RiotX_ChampionList_ezreal.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ezreal_0.jpg",

    "eb": 3150,
    "rp": 790
},{
    "nom": "Fiddlesticks",
    "surnom": "EFFROI NOCTURNE",
    "classe": "Mage",
    "poste": "Jungle",
    "description": "Quelque chose s'est éveillé à Runeterra. Quelque chose d'ancien. Quelque chose de terrible. L'horreur intemporelle connue sous le nom de Fiddlesticks arpente les frontières des civilisations mortelles, attirée dans les régions en proie à la paranoïa, où elle se repaît de ses victimes terrorisées. Armée d'une faux ébréchée, la créature décharnée moissonne la peur elle-même et fait voler en éclats l'esprit de ceux qui ont eu la malchance de lui survivre. Prenez garde aux cris des corbeaux et aux chuchotements de la silhouette qui vous paraît humaine... car Fiddlesticks est de retour.",
    "competences": ["ÉPOUVANTABLE ÉPOUVANTAIL", "TERREUR", "MOISSON FRUCTUEUSE", "FAUCHAISON", "RAFALE DE CORBEAUX"],
    "skin": ["Fiddlesticks", "Fiddlesticks Spectral", "Fiddlesticks Union Jack", "Fiddlesticks le Bandito", "Fiddlesticks Citrouille", "Fiddlesticks Clown Maléfique", "Fiddlesticks Horreur Sucrée", "Fiddlesticks Réanimé", "Fiddlesticks Prétorien"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt1151ac1242311053/5db05fbca6470d6ab91ce594/RiotX_ChampionList_fiddlesticks.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Fiddlesticks_0.jpg",

    "eb": 1350,
    "rp": 585
},{
    "nom": "Fizz",
    "surnom": "FILOU DES MERS",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "Fizz est un yordle amphibie qui vit dans les récifs qui entourent Bilgewater. Il essaie souvent de récupérer et de rendre les tributs que lancent dans l'océan les capitaines superstitieux, mais même le plus dessalé des marins n'est pas assez arrogant pour essayer de le tromper. On raconte bien des histoires sur ceux qui ont sous-estimé son humeur changeante... Souvent confondu avec un esprit océanique capricieux, il semble capable de commander aux créatures des profondeurs et n'aime rien tant que confondre alliés comme ennemis.",
    "competences": ["COMBATTANT AGILE", "FRAPPE DE L'OURSIN", "TRIDENT MARIN", "JOUEUR/FILOU", "PÊCHE AU GROS"],
    "skin": ["Fizz", "Fizz de L'Atlantide", "Fizz de la Toundra", "Fizz Pêcheur", "Fizz du Néant", "Fizz Lapin", "Super Fizz Intergalactique", "Fizz de la Section Oméga", "Fizz Toutou", "Fizz Toutou Edition Prestige", "Fizz Diablotin"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt6000fa69e03c25c0/5db05fbc56458c6b3fc17513/RiotX_ChampionList_fizz.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Fizz_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Gangplank",
    "surnom": "FLÉAU DES MERS",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Aussi imprévisible que brutal, le roi des pillards déchu connu sous le nom de Gangplank est partout redouté. Il régnait autrefois sur la ville portuaire de Bilgewater et, bien que ce temps soit révolu, certains pensent que cela l'a rendu plus dangereux encore. Gangplank préférerait engloutir une fois de plus Bilgewater dans le sang plutôt que de laisser quelqu'un d'autre s'en emparer. Il possède aujourd'hui assez de pistolets, de sabres et de barils de poudre pour prétendre récupérer ce qu'il a perdu.",
    "competences": ["ÉPREUVE DU FEU", "POURPARLERS", "GUÉRISON DU SCORBUT", "BARIL DE POUDRE", "TIR DE BARRAGE"],
    "skin": ["Gangplank", "Gangplank Lugubre", "Gangplank Minuteman", "Gangplank Matelot", "Petit Soldat Gangplank", "Gangplank des Forces Spéciales", "Gangplank Sultan", "Capitaine Gangplank", "Gangplank Nova D'Effroi", "Gangplage", "FPX Gangplank"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltfdff3781ccfb2a5c/5db05fc689fb926b491ed811/RiotX_ChampionList_gangplank.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Gangplank_0.jpg",

    "eb": 3150,
    "rp": 790
},{
    "nom": "Gnar",
    "surnom": "CHAÎNON MANQUANT",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Gnar est un yordle primitif dont les manières joyeuses peuvent soudain faire place à une colère irrationnelle. Il se transforme alors en une bête colossale portée à la destruction. Gelé dans de la glace pure pendant des millénaires, la curieuse créature s'est libérée de sa prison. Aujourd'hui, il parcourt avec enthousiasme un monde qu'il trouve exotique et merveilleux. Enchanté par le danger, Gnar lance à ses ennemis tout ce qu'il trouve, qu'il s'agisse de son boomerang en croc ou d'un bâtiment proche.",
    "competences": ["RAGE GÉNIQUE", "JET DE BOOMERANG/JET DE ROCHER", "AGITATION/BEIGNE", "REBOND/APLATISSEMENT", "GNAR !"],
    "skin": ["Gnar", "Dino Gnar", "Gnar Gentleman", "Gnar Des Neiges", "Gnar El Leon", "Super Gnar Intergalactique", "SSG Gnar", "Gnar Astronaute"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blta80f79dd96ee5d30/5db05fc6df78486c826dcd00/RiotX_ChampionList_gnar.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Gnar_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Graves",
    "surnom": "HORS-LA-LOI",
    "classe": "ADC",
    "poste": "Jungle",
    "description": "Malcolm Graves est un mercenaire, un parieur et un voleur recherché dans toutes les cités et tous les empires par lesquels il est passé. Malgré son tempérament explosif, il a un sens de l'honneur indéniable qu'il applique souvent avec son fusil à double canon, Destinée. Ces dernières années, il s'est réconcilié avec Twisted Fate. Leur partenariat rétabli, ils prospèrent à nouveau dans les allées sombres de Bilgewater.",
    "competences": ["NOUVELLE DESTINÉE", "TERMINUS", "ÉCRAN DE FUMÉE", "RUÉE VERS L'OR", "DÉGÂTS COLLATÉRAUX"],
    "skin": ["Graves", "Graves Tueur à Gages", "Graves Fugitif", "Don Graves", "Graves Anti-Emeutes", "Graves Pistolet à Eau", "Graves Coupe-Jarret", "Graves Des Neiges", "Graves Héros de Guerre", "Graves Prétorien", "Professeur Graves de L'Académie"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt2e7cd286d7b6182e/5e9a59c245a2a97194a1d4c7/RiotX_ChampionList_graves-cigar.jpg?quality=90&width=250",
    "image_back": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5796e3d258e95471/5e83cb29cfad926479634951/Graves_00_Base-Cigar.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Heimerdinger",
    "surnom": "INVENTEUR RÉPUTÉ",
    "classe": "Mage",
    "poste": "Milieu / Haut",
    "description": "Scientifique yordle aussi brillant qu'excentrique, le professeur Cecil B. Heimerdinger est considéré comme l'un des inventeurs les plus innovants qu'ait connu Piltover. Travailleur si acharné que cela confine à l'obsession, il cherche les réponses aux questions les plus obscures de l'univers. Bien que ses théories paraissent souvent opaques et ésotériques, Heimerdinger a fabriqué certaines des machineries les plus étonnantes (et les plus mortelles) de Piltover. Il ne cesse jamais de bidouiller ses inventions pour les améliorer encore.",
    "competences": ["AFFINITÉ HEXTECH", "TOURELLE H-28G ÉVOLUTION", "MICRO-ROQUETTES HEXTECH", "GRENADE ÉLECTRO-TEMPÊTE CH-2", "AMÉLIORATION !"],
    "skin": ["Heimerdinger", "Heimerdinger L'Envahisseur", "Heimerdinger Explosif", "Heimerdinger Jackys Piltover", "Snowmerdinger", "Heimerdinger Hazmat", "Heimerdinger Dragonnier", "Heimerdinger du Petit Bain"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd30d85446d154070/5db05fc57d28b76cfe4397ef/RiotX_ChampionList_heimerdinger.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Heimerdinger_0.jpg",

    "eb": 3150,
    "rp": 790
},{
    "nom": "Ivern",
    "surnom": "AÎNÉ DE LA FORÊT",
    "classe": "Support",
    "poste": "Jungle",
    "description": "Ivern Roncepied, que beaucoup connaissent sous le nom d'Aîné de la forêt, est un être étrange mi-homme mi-arbre, qui arpente les forêts de Runeterra en cultivant la vie partout où il va. Il connaît les secrets du monde naturel et entretient une profonde amitié avec tout ce qui rampe, vole et pousse. Ivern erre dans la nature sauvage, partageant sa sagesse avec tous ceux qu'il rencontre, enrichissant les forêts et confiant parfois (à tort !) certains de ses secrets aux papillons.",
    "competences": ["AMI DE LA FORÊT", "ENRACINEMENT", "MAIN VERTE", "GRAINE À RETARDEMENT", "MARGUERITE !"],
    "skin": ["Ivern", "Ivern Roi des Bonbons", "Ivern Roi du Dunk", "Ivern Sylvestre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5fff73e1df1873de/5db05fce7d28b76cfe4397f5/RiotX_ChampionList_ivern.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ivern_0.jpg",

    "eb": 6300,
    "rp": 975
},{
    "nom": "Janna",
    "surnom": "AVATAR DE L'AIR",
    "classe": "Support",
    "poste": "Support",
    "description": "Armée de la puissance des grands vents de Runeterra, Janna est un mystérieux esprit élémentaire qui protège les parias de Zaun. Certains croient que la vie lui a été donnée par les suppliques des marins de Runeterra cherchant les bons vents des mers calmes, affrontant les courants traîtres ou bravant les typhons. Depuis, on implore sa protection jusque dans les profondeurs de Zaun, où Janna est devenue un fanal d'espoir pour ceux qui sont dans le besoin. Nul ne sait quand et où elle apparaîtra, mais ceux qui espèrent son aide sont rarement déçus.",
    "competences": ["ZÉPHYR", "VENT HURLANT", "ALIZÉ", "ŒIL DU CYCLONE", "MOUSSON"],
    "skin": ["Janna", "Janna des Tempêtes", "Janna Hextech", "Janna Reine de Givre", "Janna Héroïne de Guerre", "Janna Miss Météo", "Fnatic Janna", "Janna Gardienne des Etoiles", "Janna à l'Epée Sacrée", "Janna Sorcière", "Janna Gardienne des Sables", "Janna Reine du Combat"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt02bf5329f8abe45d/5db05fcedf78486c826dcd06/RiotX_ChampionList_janna.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Janna_0.jpg",

    "eb": 1350,
    "rp": 585
},{
    "nom": "Jayce",
    "surnom": "PROTECTEUR DU FUTUR",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Jayce est un brillant inventeur qui a voué sa vie à la défense de Piltover et de sa poursuite inlassable du progrès. Armé de son marteau Hextech polymorphe, Jayce utilise aussi bien sa force que son courage et son intelligence pour protéger sa ville. Bien que considéré comme un héros par ses compatriotes, il n'apprécie pas vraiment l'attention que cela lui vaut. Jayce a néanmoins un cœur d'or, et même ceux qui sont jaloux de ses talents naturels lui sont reconnaissants de protéger la Cité du progrès.",
    "competences": ["CAPACITEUR HEXTECH", "DIRECTION LE CIEL ! / ORBE ÉLECTRIQUE", "CHAMP ÉLECTRIQUE / HYPERCHARGE", "COUP FOUDROYANT / PORTAIL D'ACCÉLÉRATION", "CANON MERCURY / MARTEAU MERCURY"],
    "skin": ["Jayce", "Full Metal Jayce", "Jayce Séducteur", "Jayce Déchu", "Jayce Marteau Radieux", "Jayce de l'Académie du Combat", "Jayce Résistant"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt51557edc36ad88a1/5db05fcf0b39e86c2f83dc13/RiotX_ChampionList_jayce.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Jayce_0.jpg",

    "eb": 4800,
    "rp": 880
},{
    "nom": "Jhin",
    "surnom": "VIRTUOSE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Jhin est un psychopathe méticuleux pour qui le meurtre est une forme d'art. Autrefois prisonnier de Ionia, mais libéré par des conspirateurs opérant au sein du conseil dirigeant du pays, ce tueur en série a désormais mis ses talents d'assassin au service de leur complot. Utilisant son pistolet comme un pinceau, Jhin crée des œuvres d'art empreintes de brutalité, aussi terrifiantes pour ses victimes que pour les témoins de ses crimes. Il tire un plaisir sadique de ses performances théâtrales, ce qui en fait un atout de poids quand il s'agit d'envoyer le plus puissant des messages : celui de la terreur.",
    "competences": ["MURMURE", "GRENADE DANSANTE", "FLORAISON MORTELLE", "PUBLIC CAPTIF", "RAPPEL DE RIDEAU"],
    "skin": ["Jhin", "Jhin de l'Ouest", "Jhin Lune de Sang", "SKT T1 Jhin", "Projet: Jhin", "Jhin Démiurge Cosmique", "Jhin des Rouleaux de Shan Hai"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltbf6c70d9272a5a2a/5db05fcfe9d7526ab429e532/RiotX_ChampionList_jhin.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Jhin_0.jpg",

    "eb": 6300,
    "rp": 975
},{
    "nom": "Jinx",
    "surnom": "GÂCHETTE FOLLE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Criminelle hystérique et impulsive de Zaun, Jinx ne vit que pour semer le chaos sans se préoccuper des conséquences. Équipée d'un arsenal d'armes mortelles, elle déchaîne les explosions les plus spectaculaires en ne laissant dans son sillage que destruction et panique. Jinx déteste s'ennuyer et c'est dans la bonne humeur qu'elle sème le chaos dans tous les endroits qu'elle traverse.",
    "competences": ["ENTHOUSIASME !", "FLIP FLAP !", "ZAP !", "PYROMÂCHEURS !", "SUPER ROQUETTE DE LA MORT !"],
    "skin": ["Jinx", "Donna Jinx", "Jinx Pyrotechnicienne", "Jinx Tueuse de Zombies", "Jinx Gardienne des Etoiles", "Jinx l'Elfe Arriviste", "Jinx de l'Odyssée", "Projet: Jinx", "Jinx Coeur de Cible"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blta2cba784d9fad4b8/5db05fce89fb926b491ed817/RiotX_ChampionList_jinx.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Jinx_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Kai'Sa",
    "surnom": "FILLE DU NÉANT",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Capturée par le Néant alors qu'elle n'était qu'une enfant, Kai'Sa parvint à survivre par la simple force de sa détermination et de sa ténacité. Si ses expériences ont fait d'elle une chasseuse meurtrière, certains voient en elle l'avant-goût d'un avenir qu'ils préféreraient ne pas connaître. Étant entrée en fragile symbiose avec une carapace vivante du Néant, il lui faudra bientôt décider si elle doit pardonner aux mortels qui la considèrent comme un monstre et vaincre la vague grandissante des ténèbres… ou si elle doit fermer les yeux et laisser le Néant consumer le monde qui l'a abandonnée.",
    "competences": ["SECONDE PEAU", "PLUIE D'ICATHIA", "RAYON DU NÉANT", "SURPRESSEUR", "INSTINCT MEURTRIER"],
    "skin": ["Kai'Sa", "Kai'Sa Ange Exterminateur", "K/DA Kai'Sa", "K/DA Kai'Sa Edition Prestige", "IG Kai'Sa", "Kai'Sa Arcade", "K/DA All Out Kai'Sa", "K/DA All Out Kai'Sa Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte38dc660dfe79204/5db05fce2dc72966da74670c/RiotX_ChampionList_kaisa.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kaisa_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Kalista",
    "surnom": "LANCE DE LA VENGEANCE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Spectre courroucé des Îles obscures, Kalista est l'esprit immortel de la vengeance, un cauchemar en armure que l'on invoque pour pourchasser les traîtres et les menteurs. Les victimes trahies peuvent réclamer vengeance, mais Kalista ne répond qu'à ceux dont elle estime la cause digne de ses talents. Malheur à ceux qui suscitent l'ire de Kalista, car tout pacte qu'elle scelle ne se finit que d'une seule façon : dans les flammes glacées de ses lances spirituelles.",
    "competences": ["MAINTIEN MARTIAL", "PERFORATION", "SENTINELLE", "EXTIRPATION", "APPEL DU DESTIN"],
    "skin": ["Kalista", "Kalista Lune de Sang", "Kalista du Championnat", "SKT T1 Kalista", "Kalista Maraudeuse"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltb7f0196921c74e8c/5db05fcee9d7526ab429e52c/RiotX_ChampionList_kalista.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kalista_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Karma",
    "surnom": "SAGESSE INCARNÉE",
    "classe": "Mage",
    "poste": "Support",
    "description": "Aucun mortel n'incarne mieux que Karma les traditions spirituelles d'Ionia. Elle est l'hôte vivant d'une âme ancienne réincarnée à de multiples reprises. Elle contient tous les souvenirs accumulés au fil de ces vies et elle possède une puissance que peu peuvent comprendre. Elle a fait de son mieux pour guider son peuple lors de la crise récente, même si elle sait que la paix et l'harmonie ne sont possibles qu'à un prix élevé, pour elle et pour la terre qu'elle aime tant.",
    "competences": ["CONCENTRATION ARDENTE", "FLAMME INTÉRIEURE", "VOLONTÉ CONCENTRÉE", "EXALTATION", "MANTRA"],
    "skin": ["Karma", "Karma Déesse du Soleil", "Karma Sakura", "Karma Classique", "Karma de l'Ordre du Lotus", "Karma Purificatrice", "Karma Merveille Hivernale", "Karma Conquérante", "Karma du Pulsar Sombre", "Karma Héraut de l'Aube", "Karma de l'Odyssée", "Karma Déchue"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt39748c7b67252d6f/5db05fd70b39e86c2f83dc19/RiotX_ChampionList_karma.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Karma_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Karthus",
    "surnom": "LICHE",
    "classe": "Mage",
    "poste": "Jungle / Milieu",
    "description": "Héraut de l'oubli, Karthus est un esprit immortel dont les chants ensorcelants préludent à l'horreur de son apparition. Les vivants craignent l'existence éternelle des morts-vivants, mais Karthus n'y voit que beauté et pureté, une union parfaite de la vie et de la mort. Lorsque Karthus émerge des Îles obscures, c'est pour apporter aux mortels la joie de la mort dont il est le vibrant apôtre.",
    "competences": ["DÉNI DE MORT", "DÉVASTATION", "MUR DE DOULEUR", "SOUILLURE", "REQUIEM"],
    "skin": ["Karthus", "Karthus Fantôme", "Statue de Karthus", "Karthus Faucheur", "Karthus Pentakill", "Fnatic Karthus", "Karthus Fléau de la Lumière", "Karthus Infernal"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt69b8fad9d1e78514/5db05fd7df78486c826dcd0c/RiotX_ChampionList_karthus.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Karthus_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Kassadin",
    "surnom": "CHASSEUR DU NÉANT",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Kassadin se fraye un chemin brûlant dans les lieux les plus sombres du monde ; il sait que ses jours sont comptés. Guide et aventurier shurimien accompli, il avait choisi de fonder une famille parmi les tribus pacifiques du sud, jusqu'au jour où son village fut ravagé par le Néant. Il jura de se venger et s'arma de reliques arcaniques qu'il combina à l'aide de technologies interdites. Enfin, Kassadin se dirigea vers les ruines d'Icathia à la recherche du prophète autoproclamé, Malzahar, prêt à affronter les monstres du Néant.",
    "competences": ["PIERRE DU NÉANT", "ORBE DU NÉANT", "LAME ÉTHÉRÉE", "PULSATION", "FISSURE"],
    "skin": ["Kassadin", "Kassadin du Festival", "Kassadin des Profondeurs", "Kassadin Humain", "Kassadin Héraut", "Kassadin Faucheur Cosmique", "Compte Kassadin", "Kassadin Hextech"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt20773f2f67e00a74/5db05fd7bd24496c390ae4ea/RiotX_ChampionList_kassadin.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kassadin_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Katarina",
    "surnom": "LAME SINISTRE",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "Aussi prompte dans ses décisions qu'elle est meurtrière en combat, Katarina figure parmi les assassins les plus prestigieux de Noxus. Fille aînée du légendaire général Du Couteau, elle révéla bien vite ses talents en éliminant ses ennemis avec la plus grande des discrétions. Son ambition était telle qu'elle n'hésitait pas à traquer des cibles solidement gardées, au risque de mettre ses alliés en danger. Quelle que fût la mission, elle n'hésitait jamais à remplir son devoir dans un tourbillon de lames acérées.",
    "competences": ["VORACITÉ", "LAME REBONDISSANTE", "PRÉPARATION", "SHUNPO", "LOTUS MORTEL"],
    "skin": ["Katarina", "Katarina Mercenaire", "Katarina Carton Rouge", "Katarina de Bilgewater", "Katarina Mistigri", "Katarina du Haut Commandement", "Katarina du Désert", "Katarina Sucre d'Orge", "Katarina du Royaume en Guerre", "Projet : Katarina", "Katarina Thanatophore", "Katarina de l'Académie du Combat", "Katarina Lune de Sang", "Katarina Reine du Combat"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltb73e3edb5937676a/5db05fd7823d426762825feb/RiotX_ChampionList_katarina.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Katarina_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Kayle",
    "surnom": "VERTUEUSE",
    "classe": "Combattant",
    "poste": "Haut / Milieu",
    "description": "Née d'une Manifestation targonienne au plus fort des Guerres runiques, Kayle honore l'héritage de sa mère en combattant pour la justice, portée par des ailes de feu divin. Elle et sa sœur jumelle Morgana ont longtemps été les protectrices de Demacia. Mais Kayle fut déçue par les trop nombreuses faiblesses des mortels et elle abandonna le royaume. Pourtant, d'après les légendes, elle punit toujours l'iniquité avec ses lames de feu, et beaucoup attendent avec espoir son retour…",
    "competences": ["ASCENSION DIVINE", "INCANDESCENCE", "BÉNÉDICTION CÉLESTE", "LAME DE FEU STELLAIRE", "JUGEMENT DIVIN"],
    "skin": ["Kayle", "Kayle D'Argent", "Kayle Viridienne", "Kayle Transcendée", "Kayle Spadassin", "Kayle Jugement", "Kayle aux Ailes Ethérées", "Kayle Anti-Emeutes", "Kayle Inquisitrice Aveugle", "Kayle Pentakill", "Kayle Psychoguerrière"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt2c7675893b5c76bc/5db05fd77d28b76cfe4397fb/RiotX_ChampionList_kayle.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kayle_0.jpg",
    
    "eb": 450,
    "rp": 260
},{
    "nom": "Kennen",
    "surnom": "CŒUR DE LA TEMPÊTE",
    "classe": "Mage",
    "poste": "Haut",
    "description": "Vif comme l'éclair, Kennen est plus que le simple protecteur de l'équilibre ionien, c'est aussi le seul yordle membre du Kinkou. En dépit de son allure de boule de poils, il est prêt à affronter n'importe quelle menace dans un tourbillon de shurikens et d'enthousiasme. Aux côtés de son maître, Shen, Kennen patrouille dans le domaine des esprits, utilisant sa dévastatrice énergie électrique pour abattre ses ennemis.",
    "competences": ["MARQUE DE TEMPÊTE", "SHURIKEN FOUDROYANT", "SURTENSION", "RUSH FOUDROYANT", "MAELSTRÖM"],
    "skin": ["Kennen", "Kennen Funeste", "Kennen des Marais", "Kennen Karatéka", "Docteur Kennen", "Kennen Arctique", "Kennen Lune de Sang", "Super Kennen", "Kennen Infernal"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc87932e656d1076e/5db05fd6adc8656c7d24e7dc/RiotX_ChampionList_kennen.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kennen_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Kindred",
    "surnom": "CHASSEURS ÉTERNELS",
    "classe": "ADC",
    "poste": "Jungle",
    "description": "Kindred représente les essences jumelles de la mort, distinctes mais indissociables. La flèche d'Agneau offre une fin rapide à ceux qui acceptent leur destin. Loup pourchasse ceux qui fuient leur dernier soupir et leur arrache brutalement la vie à coups de crocs. Bien que les interprétations de la nature de Kindred varient à travers tout Runeterra, chaque mortel doit choisir le vrai visage de sa mort.",
    "competences": ["MARQUE DE KINDRED", "DANSE DES FLÈCHES", "FRÉNÉSIE DE LOUP", "TERREUR MORTELLE", "REPOS D'AGNEAU"],
    "skin": ["Kindred", "Kindred Feu des Ombres", "Super Kindred Intergalactique", "Kindred Fleur Spirituelle"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc0f0007660b7a07b/5db05fd689fb926b491ed823/RiotX_ChampionList_kindred.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kindred_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Kog'Maw",
    "surnom": "GUEULE DES ABYSSES",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Émanant d'une brèche ouverte vers le Néant, c'est dans les ruines hantées d'Icathia que Kog'Maw, la putride créature à la gueule béante, et son insatiable curiosité firent leur apparition. Cette étrange bête du Néant doit mâchouiller tout ce qui se trouve à portée pour en comprendre la nature. Bien qu'il ne soit pas fondamentalement mauvais, Kog'Maw est dangereux par sa candeur, d'autant qu'elle précède généralement une frénésie insatiable, mue par sa curiosité sans fond.",
    "competences": ["SURPRISE D'ICATHIA", "BAVE CAUSTIQUE", "BARRAGE BIO-ARCANIQUE", "LIMON DU NÉANT", "ARTILLERIE VIVANTE"],
    "skin": ["Kog'Maw", "Kog'Maw Chenille", "Kog'Maw de Sonora", "Kog'Maw Monarque", "Kog'Maw le Renne", "Kog'Maw Lion Chinois", "Kog'Maw des Abysses", "Kog'Maw Jurassique", "Proto Kog'Maw", "Dogue'Maw", "Kog'Maw Arcaniste"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltaf483c8f812369fa/5db05fde823d426762825ff1/RiotX_ChampionList_kogmaw.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/KogMaw_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "LeBlanc",
    "surnom": "MANIPULATRICE",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "Mystérieuse, même aux yeux des autres membres de la Rose noire, LeBlanc n'est que l'un des nombreux noms de la femme pâle qui manipule le destin de Noxus depuis ses premiers jours. Utilisant sa magie pour se cloner, elle peut être partout, devant tout le monde, et même à plusieurs endroits en même temps. Les objectifs que poursuit LeBlanc, de même que sa véritable identité, sont à l'image de ses complots : totalement insaisissables.",
    "competences": ["IMAGE MIROIR", "SCEAU DE MALVEILLANCE", "DISTORSION", "CHAÎNES ÉTHÉRÉES", "IMITATION"],
    "skin": ["LeBlanc", "LeBlanc Cruelle", "LeBlanc Prestigieuse", "LeBlanc du Gui", "LeBlanc Corvus", "LeBlanc Sylvestre", "Programme LeBlanc", "IG LeBlanc", "LeBlanc de l'Assemblée", "LeBlanc du Championnat"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt4aaf881bb90b509f/5db05fde6e8b0c6d038c5cae/RiotX_ChampionList_leblanc.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Leblanc_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Lillia",
    "surnom": "FLEUR TIMIDE",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Profondément timide, Lillia, le faon féérique, parcourt nerveusement les forêts d'Ionia. Échappant toujours au regard des mortels dont la nature a longtemps captivé et intimidé la jeune créature, Lillia espère découvrir la raison pour laquelle les rêves des mortels n'atteignent plus l'Arbre des rêves. Elle sillonne désormais Ionia, sa branche magique en main, afin de retrouver les rêves inassouvis des hommes. Ce n'est qu'alors que Lillia pourra s'épanouir et aider les autres à démêler leurs peurs et trouver l'étincelle qui se cache au fond d'eux. Hii !",
    "competences": ["BÂTON CHARGÉ DE RÊVES", "FRAPPE FLEURIE", "ATTENTION, DÉSOLÉE !", "GRAINE TOURNOYANTE", "DOUCE BERCEUSE"],
    "skin": ["Lissandra", "Lillia Fleur Spirituelle"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Lillia_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Lillia_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Lissandra",
    "surnom": "SORCIÈRE DE GLACE",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "La magie de Lissandra est capable de manipuler la pure puissance de la glace en quelque chose d'à la fois obscur et terrible. Avec la force de sa glace noire, elle fait bien plus que glacer... elle empale et écrase ceux qui osent s'opposer à elle. Les habitants du Nord, terrifiés, la connaissent sous le nom de « Sorcière de glace ». La vérité est bien plus sombre : Lissandra complote pour corrompre la Nature et ramener le monde à l'âge de glace.",
    "competences": ["EMPRISE DE LA SUBLIMÉE", "ÉCLAT DE GLACE", "CERCLE DE GIVRE", "CHEMIN GLACIAL", "TOMBEAU POLAIRE"],
    "skin": ["Lissandra", "Lissandra Hémolithe", "Lissandra Reine des Lames", "Programme Lissandra", "Lissandra de l'Assemblée", "Lissandra Démiurge Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt08f869e897566c5b/5db05fdf7d28b76cfe439801/RiotX_ChampionList_lissandra.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Lissandra_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Lucian",
    "surnom": "PURIFICATEUR",
    "classe": "ADC",
    "poste": "Bas / Milieu",
    "description": "Jadis une Sentinelle de la lumière, Lucian est aujourd'hui un chasseur de morts-vivants. Il poursuit ses cibles sans répit et les éradique avec ses pistolets jumeaux. Après que Thresh a tué Senna, sa femme, Lucian s'est lancé sur la voie de la vengeance. Mais, alors même que Senna est revenue à la vie, la rage de Lucian reste inextinguible. Impitoyable et déterminé, il est prêt à tout pour protéger les vivants des horreurs indicibles de la Brume noire.",
    "competences": ["PISTOLERO", "LUMIÈRE PERFORANTE", "FLAMBOIEMENT", "POURSUITE INLASSABLE", "DÉLUGE DE BALLES"],
    "skin": ["Lucian", "Lucian Tueur à Gages", "Lucian Buteur", "Projet : Lucian", "Lucian Coeur de Cible", "Lucian de l'Ouest", "Lucian Demacia Vice", "Lucian Pulsefire", "Lucian Pulsefire Edition Prestige", "Lucian Héros de Guerre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3db0d62352b0b4f1/5db05fdf6e8b0c6d038c5cb4/RiotX_ChampionList_lucian.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Lucian_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Lulu",
    "surnom": "SORCIÈRE FÉÉRIQUE",
    "classe": "Support",
    "poste": "Support",
    "description": "Magicienne yordle, Lulu aime conjurer des illusions oniriques et de drôles de créatures en explorant Runeterra avec Pix, sa fée de compagnie. Lulu forge la réalité selon ses désirs, modifiant à sa guise la structure du monde et ce qu'elle considère comme les contraintes physiques d'un univers sans saveur. Beaucoup pensent que sa magie n'est pas naturelle, pour ne pas dire dangereuse, mais elle estime qu'un peu d'enchantement ne peut faire de mal à personne.",
    "competences": ["PIX, LE COMPAGNON FÉÉRIQUE", "DUO ÉCLATANT", "FANTAISIE", "PIX À LA RESCOUSSE !", "CROISSANCE PRODIGIEUSE"],
    "skin": ["Lulu", "Lulu Douce-Amère", "Lulu Funèbre", "Lulu Dragonnière", "Lulu Merveille Hivernale", "Molulusque", "Lulu Gardienne des Etoiles", "Lulu Enchanteresse Cosmique", "Lulu Gardienne des Pyjamas"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt424f04814efb3aca/5db05fdfe9d7526ab429e538/RiotX_ChampionList_lulu.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Lulu_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Lux",
    "surnom": "DAME DE LUMIÈRE",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Luxanna Crownguard est originaire de Demacia, un royaume isolationniste où la magie inspire la peur et la méfiance. Capable de plier la lumière à sa volonté, elle a grandi dans la crainte d'être un jour exilée et a été contrainte de dissimuler son pouvoir pour préserver l'honneur de sa famille. Cependant, l'optimisme et la ténacité de Lux l'ont conduite à accepter ses talents uniques, et elle les emploie désormais secrètement pour le bénéfice de sa patrie.",
    "competences": ["ILLUMINATION", "ENTRAVE DE LUMIÈRE", "BARRIÈRE PRISMATIQUE", "ANOMALIE RADIEUSE", "ÉCLAT FINAL"],
    "skin": ["Lux", "Lux Sorcière", "Lux Voleuse de Sorts", "Lux Commando", "Lux Impériale", "Lux de la Légion d'Acier", "Lux Gardienne des Etoiles", "Lux Elémentaliste", "Lux Impératrice Lunaire", "Lux Gardienne des Pyjamas", "Lux de l'Académie du Combat", "Lux de l'Académie du Combat Edition Prestige", "Lux Démiurge Cosmique", "Lux Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltb94b4161d8022c45/5db05fdfe9d7526ab429e53c/RiotX_ChampionList_lux.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Lux_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Malzahar",
    "surnom": "PROPHÈTE DU NÉANT",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Voyant fanatique ayant dédié son existence à l'unification de toute forme de vie, Malzahar croit fermement que le Néant est la voie du salut de Runeterra. Dans les étendues désertiques de Shurima, il suivit les voix qui murmuraient dans son esprit jusqu'à l'antique cité d'Icathia. Au plus profond des ruines, il plongea le regard dans le cœur noir du Néant et reçut de nouveaux pouvoirs, un nouveau but. Malzahar se considère désormais comme un berger : sa tâche est d'unir ses semblables… ou de libérer les créatures du Néant qui vivent sous terre.",
    "competences": ["PLAN DU NÉANT", "APPEL DU NÉANT", "NUÉE DU NÉANT", "VISIONS MALÉFIQUES", "POIGNE DU NÉANT"],
    "skin": ["Malzahar", "Malzahar Vizir", "Malzahar Prince des Ténèbres", "Malzahar Djinn", "Malzahar Tout-Puissant", "Malzahar des Neiges", "Malzahar Boss de Combat", "Malzahar Hextech", "Malzahar Brise-Monde"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd58a3a2bf5035834/5db05fde0b39e86c2f83dc1f/RiotX_ChampionList_malzahar.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Malzahar_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Miss Fortune",
    "surnom": "CHASSEUSE DE PRIMES",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Capitaine de Bilgewater réputée pour sa beauté et crainte pour sa cruauté, Sarah Fortune est une personnalité qui détonne au milieu des criminels endurcis qui arpentent la cité portuaire. Enfant, elle assista au massacre de sa famille par Gangplank, le roi des pillards, et elle prit sa revanche des années plus tard, faisant exploser son navire alors qu'il se trouvait à bord. Ceux qui la sous-estiment découvrent un adversaire imprévisible et séduisant... avant de prendre une ou deux balles dans les entrailles.",
    "competences": ["CŒUR VOLAGE", "DOUBLÉ", "FANFARONNE", "PLUIE DE BALLES", "BARRAGE DE PLOMB"],
    "skin": ["Miss Fortune", "Miss Fortune Cow-Girl", "Miss Fortune Waterloo", "Miss Fortune Agent-Secret", "Miss Fortune Sucre d'Orge", "Mad Fortune", "Miss Fortune Arcade", "Capitaine Fortune", "Miss Fortune Pistolets à Eau", "Miss Fortune Gardienne des Etoiles", "Miss Fortune Déesse des Flingues", "Miss Fortune Gardienne des Pyjamas", "Miss Fortune Sorcière", "Miss Fortune Sorcière Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltf36fa7fd7ecb59fb/5db05fe89481396d6bdd01b8/RiotX_ChampionList_missfortune.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/MissFortune_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Morgana",
    "surnom": "DÉCHUE",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Déchirée entre ses origines célestes et mortelles, Morgana a enchaîné ses ailes pour appartenir pleinement à l'humanité et abattre sa peine et sa rancœur sur les corrompus et les malhonnêtes. Elle rejette les lois et les traditions qu'elle juge injustes. Terrée dans les recoins obscurs de Demacia, elle préfère se battre pour la vérité – quand d'autres veulent la faire taire – en employant ses boucliers et ses chaînes de feu ténébreux. Plus que tout, Morgana croit fermement que même les proscrits et les parias peuvent un jour se racheter.",
    "competences": ["SIPHON D'ÂME", "ENTRAVE SOMBRE", "TOURMENT TÉNÉBREUX", "BOUCLIER NOIR", "CHAÎNES SPIRITUELLES"],
    "skin": ["Morgana", "Morgana Exilée", "Morgana Pécheresse", "Morgana La Lame Sinistre", "Morgana des Ronces", "Morgana Mariée Fantôme", "Morgana Héroïne de Guerre", "Morgana Spectre Lunaire", "Morgana Sorcière", "Morgana Impératrice Majestueuse", "Morgana de l'Assemblée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc81eece55f126d2d/5db05fe86af83b6d7032c904/RiotX_ChampionList_morgana.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Morgana_0.jpg",
    
    "eb": 1350,
    "rp": 585
},{
    "nom": "Nami",
    "surnom": "AQUAMANCIENNE",
    "classe": "Support",
    "poste": "Support",
    "description": "Jeune Vastaya des mers, Nami fut la première de la tribu des Marai à quitter les océans et à s'aventurer sur la terre ferme lorsque leur accord ancestral avec les Targoniens fut rompu. Convaincue qu'il n'y avait pas d'autre solution, elle a décidé de suivre le rituel sacré permettant de garantir la protection de son peuple. Plongée en plein chaos, Nami fait face à son avenir avec détermination, utilisant son bâton d'Aquamancienne pour invoquer la puissance des océans.",
    "competences": ["DÉFERLANTES", "PRISON AQUEUSE", "FLUX ET REFLUX", "BÉNÉDICTION DE L'AQUAMANCIENNE", "RAZ-DE-MARÉE"],
    "skin": ["Nami", "Koi Nami", "Nami Naïade", "Urf le Nami-Tin", "Nami des Abysses", "SKT T1 Nami", "Programme Nami", "Nami au Bâton Splendide", "Nami Destinée Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt13bec39f49dc35ee/5db05fe956458c6b3fc1751f/RiotX_ChampionList_nami.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Nami_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Neeko",
    "surnom": "CAMÉLÉON CURIEUX",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Originaire d'une tribu disparue de Vastayas, Neeko est capable de se fondre dans n'importe quelle foule en empruntant l'apparence des autres, allant même jusqu'à absorber un soupçon de leur état émotionnel pour différencier un instant les amis des ennemis. Personne ne sait réellement où – ni qui – peut bien être Neeko, mais ceux qui chercheront à lui faire du mal découvriront bien vite sa véritable nature… et goûteront à toute la force primordiale de son pouvoir spirituel.",
    "competences": ["MIRAGE INNÉ", "EXPLOSION FLORALE", "MÉTACLONAGE", "SPIRALE ÉPINEUSE", "FLORAISON RENVERSANTE"],
    "skin": ["Neeko", "Neeko Merveille Hivernale", "Neeko Gardienne des Etoiles", "Neeko Gardienne des Etoiles Edition Prestige", "Neeko des Rouleaux de Shan Hai"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blta62eaac6ad26a4f5/5db05fe7adc8656c7d24e7ea/RiotX_ChampionList_neeko.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Neeko_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Nidalee",
    "surnom": "CHASSERESSE BESTIALE",
    "classe": "Assassin",
    "poste": "Jungle",
    "description": "Élevée au plus profond de la jungle, Nidalee est une pisteuse d'exception qui peut se transformer à volonté en couguar. Ni totalement femme, ni totalement bête, elle défend férocement son territoire contre tous ceux qui veulent y pénétrer avec ses pièges judicieusement placés et sa lance. Elle estropie sa proie avant de se jeter sur elle sous sa forme de félin. Ceux qui survivent sont rares, et tous la décrivent comme une femme aux griffes aussi aiguisées que ses instincts...",
    "competences": ["PRÉDATEUR", "JAVELOT / MISE À TERRE", "GUÉRILLA / BOND", "CHARGE PRIMALE / TAILLADE", "ASPECT DU COUGUAR"],
    "skin": ["Nidalee", "Nidalee Lapin des Neiges", "Nidalee Léopard", "Nidalee Servante", "Pharaon Nidalee", "Nidalee Sorcière", "Nidalee Chasseuse de Têtes", "Nidalee du Royaume en Guerre", "Nidalee Challenger", "Super Nidalee Intergalactique", "Nidalee Héraut de l'Aube", "Nidalee Chasseresse Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5a2da06d413f7c15/5db05ff1df78486c826dcd18/RiotX_ChampionList_nidalee.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Nidalee_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Orianna",
    "surnom": "DEMOISELLE MÉCANIQUE",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Autrefois une jeune fille curieuse, de chair et d'os, Orianna est aujourd'hui une merveille de technologie entièrement mécanisée. Elle est tombée gravement malade à la suite d'un accident dans les quartiers inférieurs de Zaun, et son corps défaillant a été remplacé petit à petit par des prothèses perfectionnées. Accompagnée par une sphère extraordinaire qu'elle a elle-même créée pour lui servir de compagnon et de protectrice, Orianna est désormais libre d'explorer la merveilleuse ville de Piltover et ce qui se trouve au-delà.",
    "competences": ["REMONTOIR", "ORDRE : ATTAQUE", "ORDRE : DISSONANCE", "ORDRE : PROTECTION", "ORDRE : ONDE DE CHOC"],
    "skin": ["Orianna", "Orianna Gothique", "Orianna du Chaos", "Orianna Assassin", "TPA Orianna", "Orianna Merveille Hivernale", "Orianna Coeur-de-Cible", "Orianna du Pulsar Sombre", "Orianna Héroïne de Guerre", "Orianna Bouée Gonflable"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3318fc0e8103706d/5db05ff02140ec675d68f4a7/RiotX_ChampionList_orianna.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Orianna_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Quinn",
    "surnom": "AILES DE DEMACIA",
    "classe": "ADC",
    "poste": "Haut / Jungle",
    "description": "Quinn est un chevalier-éclaireur d'élite de Demacia qui se spécialise dans les missions d'infiltration en territoire ennemi. Avec son aigle de légende, Valor, elle forme un duo inséparable dont les proies rendent souvent l'âme avant même de comprendre qu'elles affrontaient non pas un, mais deux héros demaciens. D'une grande agilité quand la situation l'impose, Quinn pointe son arbalète sur l'ennemi pendant que Valor survole le terrain et marque les cibles évasives, pour une efficacité meurtrière sur le champ de bataille.",
    "competences": ["BUSARD", "ASSAUT AVEUGLANT", "ŒIL D'AIGLE", "SALTO", "EN TERRITOIRE ENNEMI"],
    "skin": ["Quinn", "Quinn Phénix", "Quinn Eclaireur Pastel", "Quinn Corsaire", "Quinn Coeur-de-Cible", "Quinn Purificatrice"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5cc3e3a189961d90/5db05ffbadc8656c7d24e7fc/RiotX_ChampionList_quinn.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Quinn_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Rakan",
    "surnom": "CHARMEUR",
    "classe": "Support",
    "poste": "Support",
    "description": "Aussi lunatique qu'il est charmeur, Rakan est un trublion vastaya tristement célèbre, et le plus grand danseur guerrier de l'histoire de la tribu Lhotla. Pour les humains qui vivent dans les hautes terres d'Ionia, son nom est depuis longtemps synonyme de festivals endiablés, de fêtes déraisonnables et de musique anarchique. Peu de gens savent cependant que cet histrion ambulant est également le partenaire de Xayah, la rebelle, dont il défend avec ferveur la cause.",
    "competences": ["PLUMES ENCHANTÉES", "RÉMIGE RAYONNANTE", "ENTRÉE TRIOMPHALE", "VALSE GUERRIÈRE", "DANSE ULTIME"],
    "skin": ["Rakan", "Rakan de l'Aube Cosmique", "Rakan Coeur Tendre", "SSG Rakan", "IG Rakan", "Rakan Gardien des Etoiles", "Rakan Sylvestre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltbe844b30961ccb7c/5db05ffb2140ec675d68f4ad/RiotX_ChampionList_rakan.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Rakan_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Rengar",
    "surnom": "FIER TRAQUEUR",
    "classe": "Assassin",
    "poste": "Haut / Jungle",
    "description": "Rengar est un féroce Vastaya chasseur de trophées qui ne vit que pour le frisson de la traque et de l'élimination des proies les plus dangereuses. Il parcourt le monde à la recherche des bêtes les plus féroces, et surtout de Kha'Zix, la créature du Néant qui lui a fait perdre un œil. Rengar ne piste ses proies ni pour la nourriture ni pour la gloire, mais pour la beauté de la poursuite en elle-même.",
    "competences": ["PRÉDATEUR INVISIBLE", "ESPRIT SAUVAGE", "RUGISSEMENT", "BOLAS", "CHASSEUR-NÉ"],
    "skin": ["Rengar", "Rengar Chasseur de Têtes", "Rengar Chasseur Nocturne", "SSW Rengar", "Mecha Rengar", "Rengar Joli Matou", "Rengar Gardien des Sables"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt282bc8e033db4123/5db05ff9adc8656c7d24e7f6/RiotX_ChampionList_rengar.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Rengar_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Rumble",
    "surnom": "MENACE MÉCANISÉE",
    "classe": "Combattant",
    "poste": "Haut / Milieu",
    "description": "Rumble est un jeune inventeur soupe au lait. Ne disposant que de ses deux mains et d'une pile de ferraille, le yordle au tempérament de feu se fabriqua une colossale armure mécanique, dotée d'un arsenal de harpons électrifiés et de roquettes incendiaires. Si certains pouffent devant ses créations sorties tout droit d'une décharge, Rumble n'en a que faire : après tout, c'est lui qui est doté d'un lance-flammes.",
    "competences": ["TITAN DE BRIC-À-BRAC", "LANCE-FLAMMES", "BOUCLIER RECYCLÉ", "HARPON ÉLECTRIQUE", "ÉRADICATION"],
    "skin": ["Rumble", "Rumble de la Jungle", "Rumble de Bilgewater", "Super Rumble Intergalactique", "Rumble Baron des Friches"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd7c76610fa29d8d6/5db05ffa7d28b76cfe439813/RiotX_ChampionList_rumble.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Rumble_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Ryze",
    "surnom": "MAGE RUNIQUE",
    "classe": "Mage",
    "poste": "Haut / Milieu",
    "description": "Largement considéré comme l'un des plus puissants sorciers de Runeterra, Ryze est un archimage endurci par les ans qui porte un fardeau incommensurable sur ses épaules. Armé d'une constitution à toute épreuve et d'un vaste arsenal de connaissances mystiques, il consacre sa vie à rassembler les Runes telluriques, les fragments de magie brute qui ont jadis servi à façonner le monde. Ryze doit récupérer ces reliques avant qu'elles ne tombent entre de mauvaises mains, car il sait quel genre d'horreurs elles pourraient déchaîner sur Runeterra.",
    "competences": ["MAÎTRISE DES ARCANES", "COURT-CIRCUIT", "PRISON RUNIQUE", "FLUX ENVOÛTANT", "PORTAIL TRANSDIMENSIONNEL"],
    "skin": ["Ryze", "Ryze Jeune", "Ryze Tribal", "Oncle Ryze", "Ryze Triomphant", "Ryze Professeur", "Ryze Zombie", "Ryze au Cristal Noir", "Ryze Pirate", "Ryze Barbe Blanche", "SKT T1 Ryze", "Ryze du Championnat", "Ryze Gardien des Sables"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt8af977ce4fa7804b/5db05ffa2dc72966da746714/RiotX_ChampionList_ryze.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ryze_0.jpg",
    
    "eb": 450,
    "rp": 270
},{
    "nom": "Samira",
    "surnom": "ROSE DU DÉSERT",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Toujours en quête de sensations fortes, Samira regarde la mort dans les yeux avec une confiance inébranlable. Après la destruction qui a frappé son foyer shurimien lorsqu'elle était enfant, Samira a trouvé sa véritable vocation à Noxus, où elle s'est forgé une réputation de tête brûlée extrêmement classe qui accepte les missions les plus dangereuses. Munie de deux pistolets à poudre noire et d'une épée personnalisée, Samira s'épanouit lorsque sa vie est en danger et élimine spectaculairement tous ceux qui se mettent en travers de sa route.",
    "competences": ["ÉLAN TÉMÉRAIRE", "PANACHE", "TOURBILLON DE LAME", "CHARGE SAUVAGE", "GÂCHETTE INFERNALE"],
    "skin": ["Samira", "Samira Psychoguerrière"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Samira_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Samira_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Senna",
    "surnom": "RÉDEMPTRICE",
    "classe": "ADC",
    "poste": "Bas / Support",
    "description": "Condamnée dès l'enfance à être hantée par la mystérieuse Brume noire, Senna rejoignit un ordre sacré connu sous le nom des Sentinelles de la lumière. Elle combattit vaillamment, avant de périr sous les coups du cruel spectre Thresh et de voir son âme emprisonnée dans sa lanterne. Refusant de perdre espoir, Senna apprit à utiliser la Brume à l'intérieur de la lanterne et revint à la vie, changée à jamais. Brandissant désormais la puissance des ténèbres comme de la lumière, Senna cherche à mettre fin à la Brume noire en la retournant contre elle-même, chaque tir de son arme permettant de libérer les âmes qui y sont perdues.",
    "competences": ["ABSOLUTION", "OMBRE PERFORANTE", "DERNIÈRE ÉTREINTE", "MALÉDICTION DE LA BRUME NOIRE", "TÉNÈBRES AVEUGLANTES"],
    "skin": ["Senna", "True Damage Senna", "True Damage Senna Edition Prestige", "Senna de l'Ouest"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt24f4735ebde3c22b/5db08d642dc72966da74686e/RiotX_ChampionList_senna.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Senna_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Séraphine",
    "surnom": "CHANTEUSE RÊVEUSE",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Née à Piltover de parents zauniens, Séraphine peut entendre les âmes des autres. Le monde chante pour elle et elle chante pour le monde. Alors que ces sons l'accablaient dans sa jeunesse, elle s'en inspire aujourd'hui et transforme le chaos en symphonie. Elle chante pour les cités sœurs, afin de rappeler à leurs citoyens qu'ils ne sont pas seuls, qu'ils sont plus forts ensemble et que, pour elle, leur potentiel est infini.",
    "competences": ["PRÉSENCE SUR SCÈNE", "NOTE AIGUË", "SON AMBIOPHONIQUE", "BATTEMENT", "BIS"],
    "skin": ["Séraphine", "K/DA ALL OUT Séraphine Indé ", "K/DA ALL OUT Séraphine Etoile Montante", "K/DA ALL OUT Séraphine Superstar"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Seraphine_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Seraphine_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Shaco",
    "surnom": "BOUFFON DES TÉNÈBRES",
    "classe": "Assassin",
    "poste": "Jungle / Support",
    "description": "Fabriqué il y a longtemps pour servir de jouet à un prince qui se sentait seul, Shaco est une marionnette enchantée qui se repaît désormais de meurtres et de chaos. Corrompu par la magie noire et la perte de sa chère mission, le pantin autrefois gentil ne se réjouit plus que du malheur des âmes qu'il tourmente. Il utilise ses jouets et ses mauvais tours pour répandre la mort et le sang, ce qui le fait follement rire. Tous ceux qui, la nuit venue, entendent un odieux ricanement savent qu'ils sont la prochaine cible du Bouffon des ténèbres.",
    "competences": ["TRAÎTRISE", "TROMPERIE", "BOÎTE SURPRISE", "POISON DOUBLE", "HALLUCINATION"],
    "skin": ["Shaco", "Shaco le Fou ", "Shaco Royal", "Shac-Noisettes", "Shaco Mécanique", "Shaco Aliéné", "Shaco Masqué", "Shaco Joker", "Shaco du Pulsar Sombre", "Shaco Arcaniste"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc8b1d1ba926d01cc/5db060036e8b0c6d038c5cba/RiotX_ChampionList_shaco.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Shaco_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Singed",
    "surnom": "CHIMISTE FOU",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Singed est un chimiste zaunien d'une intelligence exceptionnelle, prêt à tout pour repousser les limites de la connaissance, y compris y perdre sa santé mentale. Sa folie répond-elle à une logique ? Ses décoctions ont généralement l'effet escompté, mais pour beaucoup, Singed a perdu tout ce qui faisait de lui un être humain, ne laissant dans son sillage qu'une trace nocive et terrifiante.",
    "competences": ["SILLAGE TOXIQUE", "PISTE EMPOISONNÉE", "ATTRAPE-MOUCHE", "PROJECTION", "POTION DE DÉMENCE"],
    "skin": ["Singed", "Singed Anti-Emeutes ", "Singed Hextech", "Singed Surfer", "Singed Savant Fou", "Singed Techmaturgique", "Singed des Neiges", "SSW Singed", "Singed de la Peste Noire", "Singed Apiculteur", "Singed Résistant"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt766d98c27adfde28/5db06004347d1c6baa57be4f/RiotX_ChampionList_singed.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Singed_0.jpg",
    
    "eb": 450,
    "rp": 270
},{
    "nom": "Sivir",
    "surnom": "VIERGE MARTIALE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Sivir est une célèbre chasseuse de trésors et capitaine de mercenaires œuvrant dans le désert de Shurima. Armée de sa légendaire lame en croix, elle a triomphé d'innombrables batailles pour les clients prêts à honorer ses tarifs exorbitants. Connue pour sa détermination et son ambition démesurée, elle tire sa fierté des nombreux trésors qu'elle a déterrés dans les tombes les plus périlleuses de Shurima… et revendus au prix fort. Mais le jour où des forces ancestrales se sont éveillées au plus profond de Shurima, Sivir s'est retrouvée tiraillée entre deux destins conflictuels.",
    "competences": ["MILLE-PATTES", "LAME BOOMERANG", "RICOCHET", "BOUCLIER MAGIQUE", "EN CHASSE"],
    "skin": ["Sivir", "Sivir Princesse Guerrière", "Sivir Spectaculaire", "Sivir Chasseresse", "Sivir Bandit", "Sivir Pax", "Sivir du Blizzard", "Sivir Purificatrice", "Sivir Héroïne de Guerre", "Néo Sivir Pax", "Sivir Livreuse de Pizza", "Sivir de l'Odyssée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltbe8ea8bfd98ec1f3/5db06002a6470d6ab91ce5ac/RiotX_ChampionList_sivir.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sivir_0.jpg",
    
    "eb": 450,
    "rp": 270
},{
    "nom": "Sona",
    "surnom": "VIRTUOSE DE LA HARPE",
    "classe": "Support",
    "poste": "Support",
    "description": "Sona est la plus grande virtuose de l'etwahl de Demacia, ne s'exprimant que par ses accords vibrants et gracieux. Ses manières distinguées lui valent d'être appréciée par l'aristocratie, bien que beaucoup soupçonnent ses mélodies envoûtantes de s'apparenter à de la magie, tabou à Demacia. Inaudible pour des étrangers, mais comprise par ses compagnons les plus proches, Sona se sert de ses harmonies pour soigner ses alliés blessés, mais également pour frapper ses ennemis par surprise.",
    "competences": ["ACCORD DE PUISSANCE", "HYMNE À LA BRAVOURE", "ARIA DE PERSÉVÉRANCE", "MÉLODIE DE VÉLOCITÉ", "CRESCENDO"],
    "skin": ["Sona", "Sona la Muse", "Sona Pentakill", "Sona Nocturne", "Sona au Guqin", "Sona Arcade", "DJ Sona", "Sona Coeur Tendre", "Sona de l'Odyssée", "Sona Psychoguerrière"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt4810827f74fc21b9/5db06003347d1c6baa57be49/RiotX_ChampionList_sona.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sona_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Soraka",
    "surnom": "ENFANT DES ÉTOILES",
    "classe": "Support",
    "poste": "Support",
    "description": "Voyageuse originaire de dimensions célestes d'au-delà du Mont Targon, Soraka a abandonné son immortalité pour protéger les mortels de leurs instincts violents. Elle cherche à pousser tous ceux qu'elle rencontre à embrasser les vertus de la compassion et de la pitié, allant jusqu'à soigner même ceux qui lui veulent du mal. Et, malgré toutes les souffrances dont Soraka a été témoin dans ce monde, elle croit encore que le peuple de Runeterra n'a pas atteint son plein potentiel.",
    "competences": ["SALUT", "APPEL DE L'ÉTOILE", "INFUSION ASTRALE", "ÉQUINOXE", "SOUHAIT"],
    "skin": ["Soraka", "Soraka Dryade", "Soraka Divine", "Soraka du Temple", "Soraka Faucheuse", "Soraka de l'Ordre des Bananes", "Programme Soraka", "Soraka Gardiennes des Etoiles", "Soraka Gardienne des Pyjamas", "Soraka Merveille Hivernale", "Soraka Héraut de l'Aube", "Soraka Héraut de la Nuit", "Soraka Gardienne des Etoiles Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt2141839fdc483592/5db06003e9effa6ba5295fad/RiotX_ChampionList_soraka.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Soraka_0.jpg",
    
    "eb": 450,
    "rp": 270
},{
    "nom": "Swain",
    "surnom": "GRAND GÉNÉRAL NOXIEN",
    "classe": "Mage",
    "poste": "Haut / Milieu / Support",
    "description": "Jericho Swain est le chef visionnaire de Noxus, une nation expansionniste qui ne respecte que la force. Bien que déchu et blessé pendant les guerres ioniennes, il a pris le contrôle de l'empire avec une féroce détermination… Et une nouvelle main, démoniaque, a remplacé celle qu'il a perdue au combat. Désormais, Swain dirige ses armées sur la ligne de front, face à des ténèbres qu'il est le seul à percevoir à l'horizon, dans le vol des corbeaux attirés par les cadavres qui l'entourent. Dans un tourbillon de sacrifices et de regrets, le plus grand secret entre tous, c'est que le véritable ennemi est à l'intérieur.",
    "competences": ["NUÉE DE CORBEAUX", "POIGNE MORTIFÈRE", "ŒIL DE L'EMPIRE", "CAPTURE", "INCARNATION DÉMONIAQUE"],
    "skin": ["Swain", "Swain du Front du Nord", "Swain de Bilgewater", "Swain Tyran", "Swain Maître des Dragons", "Swain Hextech", "Swain Rose de Cristal"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt1a64e469280b8b9f/5db060030b39e86c2f83dc25/RiotX_ChampionList_swain.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Swain_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Sylas",
    "surnom": "RÉVOLUTIONNAIRE DÉCHAÎNÉ",
    "classe": "Mage",
    "poste": "Haut / Milieu / Jungle",
    "description": "Élevé dans l'un des quartiers les plus pauvres de Demacia, Sylas de Liebourg est devenu le symbole du côté obscur de la Grande cité. Enfant, sa capacité à déceler la sorcellerie cachée attira l'attention des tristement célèbres traqueurs de mages, qui finirent par l'emprisonner pour avoir retourné ses pouvoirs contre eux. Désormais libéré de ses chaînes, Sylas est un révolutionnaire convaincu, employant la magie de ceux qui l'entourent pour ravager le royaume qu'il a autrefois servi… Et son groupe de mages exilés semble grandir chaque jour un peu plus.",
    "competences": ["VOLÉE DE PÉTRICITE", "CROIX DU FORÇAT", "RÉGICIDE", "ÉVASION / ENLÈVEMENT", "DÉTOURNEMENT"],
    "skin": ["Sylas", "Sylas Spectre Lunaire", "Sylas de Freljord"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltf8ea29aa48fc5e35/5db0600cadc8656c7d24e808/RiotX_ChampionList_sylas.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sylas_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Syndra",
    "surnom": "SOUVERAINE OBSCURE",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Syndra est une effrayante magicienne originaire d'Ionia, aux pouvoirs impressionnants. Enfant, elle perturba les anciens de son village avec sa magie sauvage et incontrôlée. Elle fut confiée à un maître dans l'espoir qu'elle apprendrait à se contrôler, mais elle finit par découvrir que son prétendu mentor limitait ses capacités. Créant des sphères d'énergie noire avec ses sentiments de douleur et de trahison, Syndra jura de détruire quiconque tenterait de la contrôler.",
    "competences": ["TRANSCENDANCE", "SPHÈRE NOIRE", "FORCE DE LA VOLONTÉ", "DISPERSION DES FAIBLES", "DÉCHAÎNEMENT DE PUISSANCE"],
    "skin": ["Syndra", "Syndra Justicière", "Syndra de l'Atlantide", "Syndra Dame de Carreau", "Syndra des Neiges", "SKT T1 Syndra", "Syndra Gardienne des Etoiles", "Syndra Beach-Volley", "Syndra Rose Fanée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd9308eedab3a6fa5/5db0600c3a326d6df6c0e7bf/RiotX_ChampionList_syndra.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Syndra_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Taliyah",
    "surnom": "TISSEUSE DE PIERRES",
    "classe": "Mage",
    "poste": "Jungle",
    "description": "Taliyah est une mage issue des tribus nomades de Shurima, déchirée entre l'émerveillement de la jeunesse et les responsabilités des adultes. Elle a déjà traversé pratiquement tout Valoran pour découvrir la véritable nature de ses pouvoirs toujours plus puissants, avant d'être récemment retournée dans sa tribu pour la protéger. Certains se méprennent sur sa compassion et paient leur erreur au prix fort : sous son attitude joviale, Taliyah dissimule une volonté de fer capable de déplacer les montagnes et un esprit assez féroce pour faire trembler la terre.",
    "competences": ["SURF TELLURIQUE", "RAFALE FILÉE", "POUSSÉE SISMIQUE", "DÉFILAGE TELLURIQUE", "MUR DE LA TISSEUSE"],
    "skin": ["Taliyah", "Taliyah de Freljord", "SSG Taliyah", "Taliyah Surfeuse"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt35c3537198ee86b9/5db05fc52140ec675d68f49b/RiotX_ChampionList_ialiyah.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Taliyah_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Talon",
    "surnom": "LAME DES TÉNÈBRES",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "Talon est la dague cachée dans l'ombre, un assassin impitoyable, capable de frapper sans prévenir et de fuir avant que l'alerte ne soit donnée. Il a acquis une dangereuse réputation dans les brutales rues de Noxus, où il a été contraint de se battre, de tuer et de voler pour survivre. Adopté par la tristement célèbre famille Du Couteau, il use maintenant de ses talents au service de l'empire, assassinant les chefs, capitaines et héros ennemis... mais aussi tout Noxien qui aurait attiré l'ire de ses maîtres.",
    "competences": ["SAIGNÉE FATALE", "DIPLOMATIE NOXIENNE", "RATISSAGE", "VOIE DE L'ASSASSIN", "ASSAUT TÉNÉBREUX"],
    "skin": ["Talon", "Talon Renégat", "Talon Némésis", "Talon Lame du Dragon", "SSW Talon", "Talon Lune de Sang", "Talon à l'Epée Tenace", "Talon Sylvenoir", "Talon Rose Fanée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt20a737f99ebcd027/5db0600c89fb926b491ed829/RiotX_ChampionList_talon.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Talon_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Teemo",
    "surnom": "SCOUT DE BANTAM",
    "classe": "ADC",
    "poste": "Haut",
    "description": "N'ayant pas peur de braver les obstacles les plus dangereux, Teemo explore le monde avec un enthousiasme débordant et une bonne humeur contagieuse. Yordle au sens moral inflexible, il suit fièrement les préceptes du Code des éclaireurs de Bandle, parfois avec tant d'empressement qu'il en oublie les conséquences néfastes de ses actions. Pour certains, l'existence même des éclaireurs est à remettre en cause. Mais ce qu'il ne faut surtout pas remettre en cause, ce sont les convictions de Teemo !",
    "competences": ["GUÉRILLA", "FLÉCHETTE AVEUGLANTE", "VÉLOCITÉ", "TIR TOXIQUE", "PIÈGE NOCIF"],
    "skin": ["Teemo", "Teemo l'Elfe Heureux", "Teemo Eclaireur", "Teemo Blaireau", "Teemo Astronaute", "Teemo Lapin", "Super Teemo", "Teemo Panda", "Teemo de la Section Oméga", "Teemo Diablotin", "Teemo Abeille", "Teemo Fleur Spirituelle", "Teemo Fleur Spirituelle Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt30ddbbdc0549078a/5db0600da6470d6ab91ce5b8/RiotX_ChampionList_teemo.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Teemo_0.jpg",
    
    "eb": 1350,
    "rp": 585
},{
    "nom": "Thresh",
    "surnom": "GARDE AUX CHAÎNES",
    "classe": "Support",
    "poste": "Support",
    "description": "Sadique mais rusé, Thresh est un ambitieux esprit des Îles obscures. Autrefois le gardien d'innombrables secrets des arcanes, il fut terrassé par un pouvoir plus grand que la vie ou la mort, et il s'alimente maintenant des souffrances et des tourments qu'il inflige à d'autres avec une inventivité cruelle. Les souffrances de ses victimes ne se limitent pas à leur enveloppe mortelle, car Thresh fait agoniser leurs âmes, les emprisonnant pour l'éternité dans sa lanterne impie.",
    "competences": ["DAMNATION", "PEINE CAPITALE", "LIEN DES TÉNÈBRES", "FAUCHAGE", "LA CAGE"],
    "skin": ["Thresh", "Thresh des Profondeurs", "Thresh du Championnat", "Thresh Lune de Sang", "SSW Thresh", "Thresh du Pulsar Sombre", "Thresh de l'Ouest", "Thresh Pulsefire", "Thresh Pulsefire Edition Prestige", "FPX Thresh", "Thresh Fleur Spirituelle"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt46555de3bfa94d44/5db0600b2140ec675d68f4b5/RiotX_ChampionList_thresh.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Thresh_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Tristana",
    "surnom": "CANONNIÈRE YORDLE",
    "classe": "ADC",
    "poste": "Milieu / Bas",
    "description": "Si bon nombre d'autres yordles utilisent leur énergie débordante pour explorer, découvrir, inventer ou plus simplement jouer de mauvais tours, Tristana a toujours puisé son inspiration dans les récits des grands guerriers. Elle avait beaucoup entendu parler de Runeterra, de ses factions et de ses guerres, et elle pensait que les yordles aussi avaient l'étoffe des légendes. Dès son arrivée dans ce monde, elle s'équipa de son fidèle canon Boomer pour se jeter au combat avec un courage et un optimisme sans bornes.",
    "competences": ["TIR DE PRÉCISION", "TIR RAPIDE", "SAUT ROQUETTE", "CHARGE EXPLOSIVE", "TIR À IMPACT"],
    "skin": ["Tristana", "Tristana Riot Girl", "Tristana l'Elfe Sérieuse", "Tristana Pompier", "Tristana Guérilla", "Tristana Pirate", "Tristana à Propulsion", "Tristana Dragonnière", "Tristana Sorcière", "Tristana de la Section Oméga", "Tristana Diablotin", "Tristana Cosplay Pingouin"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltfbbc3205edf5207a/5db0600bdc674266df3d5d50/RiotX_ChampionList_tristana.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Tristana_0.jpg",
    
    "eb": 1350,
    "rp": 585
},{
    "nom": "Twisted Fate",
    "surnom": "MAÎTRE DES CARTES",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Twisted Fate est un virtuose des cartes à la réputation ambiguë qui a parcouru le monde entier pour l'éblouir de son charme et de ses talents, s'attirant l'admiration et la haine des riches comme des pigeons. Il prend rarement les choses au sérieux, salue chaque nouveau jour avec un sourire moqueur et insouciant. Twisted a toujours un atout dans sa manche, au propre comme au figuré.",
    "competences": ["DÉ PIPÉ", "ATOUTS", "BONNE PIOCHE", "PAQUET", "DESTINÉE"],
    "skin": ["Twisted Fate", "Twisted Fate Pax", "Twisted Fate Valet de Coeur", "Twisted Fate le Magnifique", "Twisted Fate Tango", "Twisted Fate de l'Ouest", "Twisted Fate Mousquetaire", "Twisted Fate des Enfers", "Twisted Fate Carton Rouge", "Twisted Fate Voleur à la Tire", "Twisted Fate Lune de Sang", "Twisted Fate Lune de Sang", "Twisted Fate Pulsefire", "Twisted Fate de l'Odyssée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltaebcdb8b21d440a7/5db0600ce9effa6ba5295fb3/RiotX_ChampionList_twistedfate.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/TwistedFate_0.jpg",
    
    "eb": 1350,
    "rp": 585
},{
    "nom": "Twitch",
    "surnom": "SEMEUR DE PESTE",
    "classe": "ADC",
    "poste": "Jungle / Bas",
    "description": "Rat de Zaun, pesteux de naissance, connaisseur en saleté par passion, Twitch n'a pas peur de se salir les pattes. Armé d'une arbalète chimique qu'il pointe droit sur le cœur de Piltover, Twitch s'est juré de montrer aux gens de la surface à quel point ils sont vraiment sales. Toujours furtif et insaisissable, quand il ne traîne pas dans le Puisard, il fouille dans les déchets à la recherche de trésors qu'il est le seul à apprécier... ou d'un morceau de sandwich.",
    "competences": ["VENIN MORTEL", "EMBUSCADE", "DOSE DE VENIN", "CONTAMINATION", "PANIQUE"],
    "skin": ["Twitch", "Twitch le Boss", "Twitch de Whistler", "Twitch Médiéval", "Twitch Gangster", "Twitch Vandale", "Twitch Pickpocket", "SSG Twitch", "Twitch de la Section Oméga", "Twitch Roi des Glaces", "Twitch Sombrepas"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt674dca7d5611ebb0/5db060159481396d6bdd01be/RiotX_ChampionList_twitch.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Twitch_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Varus",
    "surnom": "FLÈCHE DE LA VENGEANCE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Membre de la race antique des Darkin, Varus était en son temps un assassin cruel qui aimait torturer ses ennemis, les conduisant jusqu'aux portes de la folie avant de les achever d'une flèche. À la fin de la Guerre des Darkin, il fut emprisonné. Des siècles plus tard, il s'évada en utilisant la chair recomposée de deux chasseurs d'Ionia : l'ayant relâché par imprudence, ils furent condamnés à porter l'arc lié à l'essence de Varus. Varus traque désormais ceux qui l'ont enfermé pour se venger, mais les âmes des mortels à l'intérieur de lui s'opposent à ses desseins de toutes leurs forces.",
    "competences": ["VENGEANCE INCARNÉEL", "FLÈCHE PERFORANTE", "CARQUOIS MEURTRI", "PLUIE DE FLÈCHES", "CHAÎNE CORRUPTRICE"],
    "skin": ["Varus", "Varus au Cristal Impur", "Varus à l'Arc Pur", "Varus Arctique", "Varus Coeur de Cible", "Varus Rôdeur", "Varus du Pulsar Sombre", "Varus Conquérant", "Varus Infernal", "Varus Chasseur Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt596158d85a8360d1/5db060132dc72966da74671a/RiotX_ChampionList_varus.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Varus_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Vayne",
    "surnom": "CHASSEUR NOCTURNE",
    "classe": "ADC",
    "poste": "Haut / Bas",
    "description": "Shauna Vayne est une chasseuse de monstres mortelle et sans scrupule. Originaire de Demacia, elle voue sa vie à la destruction du démon qui a assassiné sa famille. Armée d'une arbalète montée sur son poignet et dotée d'un cœur rongé par le désir de vengeance, elle n'est heureuse que quand elle assassine les praticiens ou les créations des arts obscurs en les frappant depuis l'ombre avec une volée de carreaux d'argent.",
    "competences": ["CHASSEUR NOCTURNE", "ROULADE", "CARREAUX D'ARGENT", "CONDAMNATION", "COMBAT ULTIME"],
    "skin": ["Vayne", "Vayne Justicière", "Vayne Aristocrate", "Vayne Tueuse de Dragons", "Vayne Coeur de Cible", "Vayne à l'Arbalète Pure", "Vayne Voleuse d'Ames", "Projet : Vayne", "Vayne Pyrotechnicienne", "Vayne Pyrotechnicienne Edition Prestige", "Vayne Fleur Spirituelle", "FPX Vayne"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt9eaabc908956c8b0/5db060146af83b6d7032c90a/RiotX_ChampionList_vayne.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Vayne_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Veigar",
    "surnom": "SEIGNEUR DES MALÉFICES",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Maître enthousiaste de la magie noire, Veigar s'est approprié des pouvoirs que peu de mortels osent approcher. Habitant de Bandle à l'esprit libre, il veut repousser les limites de la magie yordle et se tourner vers les textes arcaniques qui sont restés cachés des millénaires. Fasciné jusqu'à l'obstination par les mystères de l'univers, Veigar est souvent sous-estimé par les gens qui l'entourent. Bien qu'il se croie fondamentalement maléfique, il possède une moralité qui le mène à s'interroger sur ses motivations.",
    "competences": ["POUVOIR MALÉFIQUE PHÉNOMÉNAL", "COUP MALIN", "MATIÈRE NOIRE", "PROFANATION", "EXPLOSION PRIMORDIALE"],
    "skin": ["Veigar", "Veigar le Mage Blanc", "Veigar Curling", "Veigar Barbe Grise", "Veigar Lutin", "Baron Von Veigar", "Veigar Classieux", "Maléfique Père Veigar", "Veigar Boss de Fin", "Veigar de la Section Oméga", "Veigar Sylvestre", "Veigar Cosplay Corgnon"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltda2b568b0c3e5847/5db06014e9effa6ba5295fb9/RiotX_ChampionList_veigar.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Veigar_0.jpg",
    
    "eb": 1350,
    "rp": 575
},{
    "nom": "Vel'Koz",
    "surnom": "ŒIL DU NÉANT",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "On ne sait pas trop si Vel'Koz fut le premier monstre du Néant à émerger à Runeterra, mais il est certain qu'aucun autre n'a jamais égalé la froideur calculatrice de sa cruauté. Alors que ses semblables dévorent ou profanent tout ce qui les entoure, il cherche à étudier le royaume physique et les étranges êtres belliqueux qui l'habitent. Selon lui, c'est ainsi que le Néant trouvera comment exploiter leurs faiblesses. Mais Vel'Koz n'est pas qu'un observateur passif. Il répond à toute menace avec des éruptions mortelles de plasma ou en déchirant le tissu même du monde.",
    "competences": ["DÉCOMPOSITION ORGANIQUE", "FISSION DU PLASMA", "OUVERTURE DE FAILLE", "DISLOCATION TECTONIQUE", "DÉSINTÉGRATEUR DE FORMES DE VIE"],
    "skin": ["Vel'Koz", "Proto Vel'Koz", "Vel'Koz aux Rayons Purs", "Vel'Koz Incognito", "Vel'Koz Infernal"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt825d0c333f6e74ae/5db060142140ec675d68f4bb/RiotX_ChampionList_velkoz.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Velkoz_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Viktor",
    "surnom": "HÉRAUT DES MACHINES",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Héraut d'un nouvel âge de technologie, Viktor a consacré sa vie aux progrès de l'humanité. Idéaliste cherchant à hisser le peuple de Zaun à de nouveaux sommets de compréhension, il pense que le potentiel de l'humanité ne pourra être réalisé que par le grand pas en avant d'une glorieuse évolution technologique. Doté d'un corps optimisé par l'acier et la science, Viktor poursuit fanatiquement son idéal d'un meilleur avenir.",
    "competences": ["GLORIEUSE ÉVOLUTION", "SIPHONNAGE", "CHAMP GRAVITATIONNEL", "RAYON DE LA MORT", "TEMPÊTE DU CHAOS"],
    "skin": ["Viktor", "Viktor Ex Machina", "Viktor Prototype", "Viktor Créateur", "Viktor Thanatophore", "Viktor Psychoguerrier"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt6e54b3de49b7bbf3/5db06015df78486c826dcd1e/RiotX_ChampionList_viktor.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Viktor_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Vladimir",
    "surnom": "SAIGNEUR POURPRE",
    "classe": "Mage",
    "poste": "Haut / Milieu",
    "description": "Assoiffé de sang humain, Vladimir influe sur la politique de Noxus depuis les premiers jours de l'empire. En plus d'allonger surnaturellement sa vie, sa maîtrise de l'hémomancie lui permet de contrôler les esprits et les corps des autres aussi aisément que les siens. Dans les flamboyants salons de l'aristocratie noxienne, cela lui a permis de créer autour de lui un culte de la personnalité. Cependant, dans les bas-fonds, il saigne littéralement ses ennemis à blanc.",
    "competences": ["PACTE DE SANG", "TRANSFUSION", "BAIN DE SANG", "VAGUES DE SANG", "PESTE SANGUINE"],
    "skin": ["Vladimir", "Compte Vladimir", "Marquis Vladimir", "Vladimir Nosferatu", "Vladimir Biker", "Vladimir Sanguinaire", "Vladimir Voleur d'Ames", "Vladiplômé", "Vladimir des Eaux Sombres", "Vladimir Héraut de la Nuit", "Vladimir Dévoreur Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt39ab5027f6fa1b81/5db0601589fb926b491ed82f/RiotX_ChampionList_vladimir.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Vladimir_0.jpg",
    
    "eb": 3150,
    "rp": 790
},{
    "nom": "Xayah",
    "surnom": "REBELLE",
    "classe": "ADC",
    "poste": "Bas",
    "description": "Précise et meurtrière, Xayah est une révolutionnaire vastaya menant une guerre personnelle dans le but de sauver son peuple. Elle se sert de sa célérité, de sa ruse et de ses plumes tranchantes comme des rasoirs pour terrasser tous ceux qui se dressent sur sa route. Xayah se bat aux côtés de son partenaire et amant, Rakan, pour défendre leur tribu dépérissante et rendre à leur race ce qu'elle considère être sa gloire d'antan.",
    "competences": ["PLUMES PERÇANTES", "DAGUES JUMELLES", "PLUMAGE MORTEL", "APPEL DES LAMES", "RAFALE DE PLUMES"],
    "skin": ["Xayah", "Xayah du Crépuscule Cosmique", "Xayah Coeur Tendre", "SSG Xayah", "Xayah Gardienne des Etoiles", "Xayah Sylvestre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt0d029ccdb18a4299/5db0601ba6470d6ab91ce5be/RiotX_ChampionList_xayah.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Xayah_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Xerath",
    "surnom": "MAGE SUPRÊME",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Xerath est un mage transfiguré de la Shurima antique, un être d'énergie arcanique habitant les fragments détruits d'un sarcophage magique. Pendant des millénaires, il est resté prisonnier des sables du désert, mais le retour de Shurima l'a libéré de sa prison ancestrale. Devenu ivre de pouvoir, il cherche désormais à reprendre ce qu'il croit lui être dû et à remplacer les civilisations arrivistes qui ont pris possession du monde par une nouvelle, façonnée à son image.",
    "competences": ["AFFLUX DE MANA", "RAYON ARCANIQUE", "ŒIL DE LA DESTRUCTION", "ORBE D'ÉLECTROCUTION", "RITE ARCANIQUE"],
    "skin": ["Xerath", "Xerath Runique", "Proto Xerath", "Xerath de la Terre Brûlée", "Xerath Gardien des Sables", "Xerath du Pulsar Sombre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltbda694c8890e94e5/5db0601ce9effa6ba5295fbf/RiotX_ChampionList_xeratth.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Xerath_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Yuumi",
    "surnom": "GARDIENNE DU GRIMOIRE",
    "classe": "Support",
    "poste": "Support",
    "description": "Originaire de Bandle, Yuumi est un familier magique dont la maîtresse, une enchanteresse yordle du nom de Norra, a mystérieusement disparu. Yuumi est désormais la gardienne du Grimoire des Seuils que possédait Norra, un livre permettant de franchir des portails. Partie à la recherche de sa maîtresse, Yuumi s'est fait de nouveaux amis durant son périple, et elle les protège tous avec la même détermination. Même si Grimoire aimerait que Yuumi se concentre davantage sur sa mission, sa comparse aime prendre le temps de faire la sieste et de pêcher des poissons. Malgré cela, elle finit toujours par repartir en quête de sa maîtresse.",
    "competences": ["PAS TOUCHE", "TÊTE CHAT-SSEUSE", "FÉLIN POUR L'AUTRE !", "ZOUUU !", "CHAT-PITRE FINAL"],
    "skin": ["Yuumi", "Proviseur Yuumi de l'Académie", "Yuumi Coeur de Cible"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blta312e7cca0e594d1/5db0601d2140ec675d68f4c1/RiotX_ChampionList_yuumi.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yuumi_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Zed",
    "surnom": "MAÎTRE DES OMBRES",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "L'impitoyable Zed est le maître de l'Ordre de l'ombre, une organisation qu'il a créée dans l'intention de militariser la tradition des arts martiaux d'Ionia et de chasser enfin les envahisseurs noxiens. Pendant la guerre, le désespoir le conduisit à déchaîner la Forme secrète de l'ombre, un puissant esprit maléfique aussi dangereux que corrompu. Si Zed maîtrise ces techniques interdites, c'est pour détruire tout ce qu'il considère comme une menace contre sa nation ou son ordre.",
    "competences": ["MÉPRIS DES FAIBLES", "SHURIKEN-RASOIR", "OMBRE VIVANTE", "TAILLADE DES OMBRES", "MARQUE DE LA MORT"],
    "skin": ["Zed", "Zed l'Eclair Blanc", "SKT T1 Zed", "Projet : Zed", "Zed du Championnat", "Zed Thanatophore", "Zed Fléau Galactique", "Zed Psychoguerrier"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt402d6da485218720/5db0601de9effa6ba5295fc5/RiotX_ChampionList_zed.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Zed_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Ziggs",
    "surnom": "EXPERT DES HEXPLOSIFS",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Amoureux des grosses bombes et des mèches courtes, le yordle Ziggs est une force de la nature explosive. Assistant d'un inventeur de Piltover, il finit par se lasser de sa vie routinière et devint l'ami d'une jeune fille imprévisible et portée sur les explosifs, Jinx. Après une folle nuit en ville, Ziggs suivit les conseils de Jinx et s'installa à Zaun, où il se livre désormais à sa passion plus librement, terrorisant les Barons de la chimie comme les simples citoyens dans sa quête éternelle pour faire sauter tout ce qui traverse son champ de vision.",
    "competences": ["MÈCHE COURTE", "BOMBE REBONDISSANTE", "CHARGE EXPLOSIVE", "MINES HEXPLOSIVES", "MÉGA BOMBE INFERNALE"],
    "skin": ["Ziggs", "Ziggs Savant Fou", "Comandant Ziggs", "Ziggs Bombe à Eau", "Ziggs des Neiges", "Ziggs Maître Arcaniste", "Ziggs Boss de Combat", "Ziggs de l'Odyssée", "Ziggs Folie Sucrée", "Ziggs Hextech"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt0f8c12d54d8ecd28/5db0602289fb926b491ed835/RiotX_ChampionList_ziggs.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ziggs_0.jpg",
    
    "eb": 4800,
    "rp": 880
},{
    "nom": "Zilean",
    "surnom": "GARDIEN DU TEMPS",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Naguère puissant mage d'Icathia, Zilean devint obsédé par le passage du temps après avoir assisté à la destruction de sa terre natale par le Néant. Refusant de perdre une minute à pleurer sur les décombres, il fit appel à une antique force magique capable de prédire tous les avenirs possibles. Devenu techniquement immortel, Zilean erre désormais entre passé, présent et futur, distordant autour de lui le flux du temps, toujours à la recherche du moment exact qui lui permettra de changer le passé et d'empêcher la destruction d'Icathia.",
    "competences": ["TEMPS DANS UNE BOUTEILLE", "BOMBE À RETARDEMENT", "RETOUR RAPIDE", "DISTORSION TEMPORELLE", "RETOUR TEMPOREL"],
    "skin": ["Zilean", "Vieux Saint Zilean", "Zilean Groovy", "Zilean du Désert de Shurima", "Zilean Lune de Sang", "Zilean Folie Sucrée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blta32de59397f53325/5db060222dc72966da746720/RiotX_ChampionList_zilean.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Zilean_0.jpg",
    
    "eb": 1350,
    "rp": 585
},{
    "nom": "Zoé",
    "surnom": "MANIFESTATION DU CRÉPUSCULE",
    "classe": "Mage",
    "poste": "Milieu",
    "description": "Incarnation de l'espièglerie, de l'imagination et de l'inconstance, Zoé est une messagère cosmique de Targon, annonciatrice des grands événements qui chamboulent les mondes. Sa simple présence distord les règles mathématiques qui gouvernent dans l'ombre les réalités et, sans pour autant le vouloir, peut même provoquer des cataclysmes. Cela explique peut-être la nonchalance désinvolte avec laquelle Zoé traite ses tâches, préférant consacrer le plus clair de son temps à s'amuser, à jouer des tours aux mortels, bref, à faire tout ce qui lui plaît. Une rencontre avec Zoé peut se révéler joyeuse et porteuse d'optimisme, mais ce n'est souvent qu'une apparence, cachant une vérité extrêmement dangereuse.",
    "competences": ["PLUS D'ÉTINCELLES !", "ASTRO-PONG !", "VOLEUSE DE SORTS", "BULLE SOPORIFIQUE", "BOND DIMENSIONNEL"],
    "skin": ["Zoé", "Zoé Cyberpop", "Zoé du Petit Bain", "Zoé Gardienne des Etoiles", "Zoé Arcaniste", "Zoé Arcaniste Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd18587f31803441d/5db060226e8b0c6d038c5cc6/RiotX_ChampionList_zoe.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Zoe_0.jpg",
    
    "eb": 6300,
    "rp": 975
},{
    "nom": "Zyra",
    "surnom": "MANIFESTATION DU CRÉPUSCULE",
    "classe": "Mage",
    "poste": "Milieu / Support",
    "description": "Née au cours d'une ancienne catastrophe magique, Zyra est la colère de la nature incarnée : mélange de plante et d'humain, faisant émerger de nouvelles pousses à chaque pas. Elle considère les nombreux mortels de Valoran comme de simples proies pour sa progéniture végétale, et les éliminer avec ses gerbes d'épines ne lui pose aucun problème. Bien que nul ne connaisse ses vrais objectifs, Zyra erre de par le monde, suivant son instinct qui est de coloniser toujours plus de terres et d'en exterminer toutes les autres formes de vie.",
    "competences": ["JARDIN DE RONCES", "ÉPINES FUNESTES", "CROISSANCE INCONTRÔLÉE", "RACINES FIXATRICES", "RONCES ÉTRANGLEUSES"],
    "skin": ["Zyra", "Zyra magmatique", "Zyra Hantée", "SKT T1 Zyra", "Zyra Sorcière Draconique", "Zyra de l'Assemblée", "Zyra de l'Assemblée Edition Prestige", "Zyra Rose de Cristal"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt9bc3497cdd04f6d5/5db060229481396d6bdd01c4/RiotX_ChampionList_zyra.jpg?quality=90&width=250",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Zyra_0.jpg",
    
    "eb": 4800,
    "rp": 880
}


mongo.db.leagueofstat.insert_many(champion)