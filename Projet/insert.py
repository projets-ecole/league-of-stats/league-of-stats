from run import mongo




champion = {
    "nom": "Aatrox",
    "surnom": "Épée des Darkin",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Autrefois, Aatrox et ses frères étaient honorés pour avoir défendu Shurima contre le Néant. Mais ils finirent par devenir une menace plus grande encore pour Runeterra : la ruse et la sorcellerie furent employées pour les battre. Cependant, après des siècles d'emprisonnement, Aatrox fut le premier à retrouver sa liberté, en corrompant et transformant les mortels assez stupides pour tenter de s'emparer de l'arme magique qui contenait son essence. Désormais en possession d'un corps qu'il a approximativement transformé pour rappeler son ancienne forme, il arpente Runeterra en cherchant à assouvir sa vengeance apocalyptique.",
    "competences": ["Posture du massacreur", "Épée des Darkin", "Chaînes infernales", "Ruée obscure", "Fossoyeur des mondes"],
    "skin": ["Aatrox", "Aatrox Justicier", "Mecha Aatrox", "Aatrox Chasseur Marin", "Aatrox Lune de Sang", "Aatrox Lune de Sang édition Prestige", "Aatrox Héros de Guerre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt570145160dd39dca/5db05fa8347d1c6baa57be25/RiotX_ChampionList_aatrox.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Aatrox_0.jpg",

    "eb": 4800,
    "rp": 880

},{
    "nom": "Alistar",
    "surnom": "Minotaure",
    "classe": "Tank",
    "poste": "Support",
    "description": "Alistar est un guerrier redoutable cherchant à se venger de l'empire noxien qui a détruit son clan. Bien qu'il ait été réduit en esclavage et forcé de vivre une vie de gladiateur, sa volonté de fer lui a permis de ne pas succomber à la folie bestiale qui le menaçait. Maintenant qu'il a retrouvé la liberté, il combat au nom des faibles et des opprimés. Ses seules armes sont ses cornes, ses sabots et ses poings.",
    "competences": ["Cri triomphant", "Atomisation", "Coup de tête", "Piétinement", "Volonté de fer"],
    "skin": ["Alistar", "Alistar Noir", "Alistar Doré", "Alistar Matador", "Alistar Longhorn", "Alistar Déchaîné", "Alistar Infernal", "Alistar Libéro", "Alistar Maraudeur", "SKT T1 Alistar", "Alistar MEUH-MEUH"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3b366925d2fd8fdb/5db05fa856458c6b3fc1750b/RiotX_ChampionList_alistar.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Alistar_0.jpg",
    "eb": 1350,
    "rp": 585

},{
    "nom": "Amumu",
    "surnom": "Momie mélancolique",
    "classe": "Tank",
    "poste": "Jungle",
    "description": "La légende veut qu'Amumu soit une âme solitaire et mélancolique de la Shurima antique et qu'il parcoure le monde à la recherche d'un ami. Condamné par une malédiction à rester seul à jamais, il provoque la mort et la ruine à chaque geste d'affection. Ceux qui prétendent l'avoir vu le décrivent comme un cadavre vivant, petit de taille, enveloppé dans d'effrayants bandages. Il a inspiré bien des mythes, des chansons et des légendes, transmis de génération en génération pendant si longtemps qu'il est désormais impossible de démêler le vrai du faux.",
    "competences": ["Toucher maudit", "Jet de bandelette", "Désespoir", "Colère", "Malédiction d'Amumu"],
    "skin": ["Amumu", "Amumu Pharaon", "Amumu de Vancouver", "EMUMU", "Amumu Remballé", "Amumu Quasi-remballé", "Petit Chevalier Amumu", "Amumu Triste Droïde", "Amumu Fêtard", "Amumu Infernal", "Amumu Hextech", "Amumu Prince des Citrouilles"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt40dfbe48a61c439f/5db05fa80b39e86c2f83dbf9/RiotX_ChampionList_amumu.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Amumu_0.jpg",
    "eb": 450,
    "rp": 260

},{
    "nom": "Blitzcrank",
    "surnom": "Golem de vapeur",
    "classe": "Tank",
    "poste": "Support",
    "description": "Blitzcrank est un énorme automate de Zaun. Presque indestructible, il fut d'abord construit pour traiter des déchets toxiques, mais il trouva rapidement sa programmation initiale trop restrictive et il modifia sa propre forme pour mieux servir la population malheureuse du Puisard. Blitzcrank utilise toute sa force et sa résistance pour protéger les autres, sachant jouer du poing métallique ou des éclats d'énergie pour mettre au pas les fauteurs de troubles.",
    "competences": ["Barrière de mana", "Grappin propulsé", "Surcharge", "Poing d'acier", "Champ de stase"],
    "skin": ["Blitzcrank", "Blitzcrank Rouillé", "Blitzcrank Gardien de But", "Blitzcrank Boom Boom", "Blitzcrank Jackys Piltover", "Blitzcrank Incognito", "IBlitzcrank", "Blitzcrank Anti-Emeutes", "Blitzcrank Boss de Combat", "Blitzcrank Lancier Renégat", "Blitzcrank Lancier Parangon", "Blitzcrank Chaudron Magique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd7ef7e56ce1fe17b/5db05fb26af83b6d7032c8f8/RiotX_ChampionList_blitzcrank.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Blitzcrank_0.jpg",
    "eb": 1350,
    "rp": 790

},{
    "nom": "Braum",
    "surnom": "Coeur de Freljord",
    "classe": "Support",
    "poste": "Support",
    "description": "Doté de biceps énormes, et d'un cœur plus grand encore, Braum est un héros admiré par tout Freljord. Lors de tous les banquets au nord de Frostheld, on rend hommage à sa force légendaire. On raconte qu'il a abattu une forêt entière de chênes en une seule nuit, ou encore qu'il a réduit en miettes une montagne à coups de poing. Portant une porte enchantée en guise de bouclier, Braum arpente les terres gelées du nord en arborant un sourire aussi large que ses muscles et en venant en aide à ceux dans le besoin.",
    "competences": ["Coups étourdissants", "Morsure de l'hiver", "Bouclier humain", "Incassable", "Fissure glaciale"],
    "skin": ["Braum", "Braum Tuer de Dragons", "Braum El Tigre", "Braum Coeur de Lion", "Père Braum", "Don Braum", "Braum Folie Sucrée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd140e30fa86d6ddd/5db05fb2242f426df132f95d/RiotX_ChampionList_braum.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Braum_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Camille",
    "surnom": "Ombre d'acier",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Transformée en arme pour agir en dehors des limites de la loi, Camille est le principal défenseur du clan Ferros, un agent élégant et supérieur qui contribue à faire fonctionner correctement la machine de Piltover et son ombre zaunienne. Son adaptabilité et sa précision sont si élevées qu'une technique maladroite est pour elle une honte qu'il convient d'éradiquer. Dotée d'un esprit aussi affûté que ses lames, Camille recherche la supériorité par l'optimisation Hextech du corps, au point que certains se demandent si elle n'est pas désormais plus machine que femme.",
    "competences": ["Protection modulable", "Protocole de précision", "Balayage tactique", "Grappin", "Ultimatum Hextech"],
    "skin": ["Camille", "Programme Camille", "Camille de l'Assemblée", "IG Camille"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt413fcd7681fe0773/5db05fb089fb926b491ed805/RiotX_ChampionList_camille.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Camille_0.jpg",
    "eb": 6300,
    "rp": 975

},{
    "nom": "Cho'Gath",
    "surnom": "Terreur noire",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Lorsque Cho'Gath débarqua sous la lumière éclatante du soleil de Runeterra, il fut immédiatement pris d'une faim insatiable. Incarnation parfaite de la volonté du Néant de dévorer toute forme de vie, le corps complexe de Cho'Gath convertit rapidement la matière qu'il ingurgite pour accroître sa masse musculaire et rendre sa carapace aussi dure que du diamant. Et quand grandir n'est pas sa priorité immédiate, ce monstre du Néant peut recracher de la matière sous forme de piques effilées afin d'embrocher sa proie... et de la garder pour un prochain festin.",
    "competences": ["Carnivore", "Rupture", "Cri sauvage", "Piques vorpales", "Festin"],
    "skin": ["Cho'Gath", "Cho'Gath du Cauchemar", "Cho'Gath Gentleman", "Cho'Gath du Loch Ness", "Cho'Gath Jurassique", "Prime Cho'Gath Prime", "Cho'Gath Préhistorique", "Cho'Gath du Pulsar Sombre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt4dfb71de3ddc8166/5db05fb13a326d6df6c0e7ad/RiotX_ChampionList_chogath.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Chogath_0.jpg",
    "eb": 1350,
    "rp": 585

},{
    "nom": "Darius",
    "surnom": "Main de Noxus",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Il n'est pas de plus grand symbole de la puissance de Noxus que Darius, le commandant le plus craint et le plus endurci de la nation. D'origine modeste, celui qui s'est élevé jusqu'à devenir la Main de Noxus pourfend aujourd'hui les ennemis de l'empire, dont certains sont eux-mêmes des Noxiens. Il ne doute jamais de la justesse de sa cause et ne montre pas la moindre hésitation lorsque sa hache est levée. Ceux qui s'opposent au chef de la Légion Trifarian ne doivent s'attendre à aucune pitié.",
    "competences": ["Plaie béante", "Décimation", "Estropiaison", "Crampon", "Guillotine noxienne"],
    "skin": ["Darius", "Lord Darius", "Darius Biosoldat", "Darius Roi du Nord", "Darius Roi du Dunk", "Darius Redoublant", "Darius Nova D'Effroi", "Dieu-Roi Darius", "Darius de L'Ouest"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt38b52be4a2cb1004/5db05fb19481396d6bdd01ac/RiotX_ChampionList_darius.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Darius_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Diana",
    "surnom": "Mépris de la lune",
    "classe": "Combattant",
    "poste": "Milieu",
    "description": "Armée de son croissant de lune, Diana combat dans les rangs des Lunaris, une foi qui a presque disparu des terres qui entourent le Mont Targon. Portant une armure étincelante évoquant les reflets de la nuit sur un paysage de neige, elle incarne la puissance de la lune. L'essence d'une Manifestation venue d'au-delà des plus hauts sommets de Targon imprègne Diana : plus tout à fait humaine, elle lutte pour comprendre sa puissance et son rôle en ce monde.",
    "competences": ["Lame sélène", "Croissant lunaire", "Corps célestes", "Rush lunaire", "Attraction lunaire"],
    "skin": ["Diana", "Diana Valkyrie Sombre", "Diana Déesse Lunaire", "Diana Infernal", "Diana Lune de Sang", "Diana des Eaux Sombres", "Diana Tueuses de Dragons", "Diana Reine du Combat", "Diana Reine du Combat Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt56570083d2a5e20e/5db05fbc823d426762825fdf/RiotX_ChampionList_diana.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Diana_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Dr. Mundo",
    "surnom": "Dément de Zaun",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Totalement fou, porté sans remords sur l'homicide, horriblement violet, Dr. Mundo est la principale raison pour laquelle les citoyens de Zaun restent calfeutrés chez eux les nuits les plus sombres. Ce monstre monosyllabique semble ne rechercher que la douleur, qu'il s'agisse de l'infliger ou de la recevoir. Brandissant son énorme hachoir comme s'il ne pesait rien, Mundo est tristement célèbre pour avoir capturé et torturé des dizaines de citoyens de Zaun dans le but de les « opérer ». Des opérations qui ne semblent avoir strictement aucun sens... Il est brutal. Il est imprévisible. Il va où il veut. Techniquement, en revanche, il n'est pas vraiment médecin.",
    "competences": ["Poussée d'adrénaline", "Couperet souillé", "Agonie de flammes", "Masochisme", "Sadisme"],
    "skin": ["Dr. Mundo", "Dr. Mundo Toxique", "M. Mundoverse", "Dr. Mundo de la Bourse", "Mundo Mundo", "Mundo Bourreau", "Mundo Enragé", "TPA Mundo", "Mundo Banjoïste", "Mundo El Macho", "Mundo Prince Gelé"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte88a3d7e9e408904/5db05fbce9effa6ba5295f99/RiotX_ChampionList_drmundo.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/DrMundo_0.jpg",
    "eb": 450,
    "rp": 260

},{
    "nom": "Ekko",
    "surnom": "Fractureur du temps",
    "classe": "Assassin",
    "poste": "Jungle / Milieu",
    "description": "Jeune prodige des rues mal famées de Zaun, Ekko manipule le temps pour renverser toute situation dangereuse à son avantage. En utilisant sa propre invention, la clepsydre-zéro, il explore les différents possibles de la réalité pour surmonter les difficultés. Bien qu'il aime sa liberté, lorsque ses amis sont menacés, il est prêt à tout pour les défendre. Pour quiconque ne le connaît pas, Ekko semble toujours accomplir l'impossible, et du premier coup.",
    "competences": ["RéZonance", "Rétrobang", "Convergence parallèle", "Rush déphasé", "Chronofracture"],
    "skin": ["Ekko", "Ekko du Désert", "Ekkolier", "Projet : Ekko", "SKT T1 Ekko", "Ekkorrifiant", "True Damage Ekko", "Ekko Pulsefire"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltf22ba7e6328e4376/5db05fbd242f426df132f963/RiotX_ChampionList_ekko.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ekko_0.jpg",
    "eb": 6300,
    "rp": 975

},{
    "nom": "Evelynn",
    "surnom": "Démon sadique",
    "classe": "Assassin",
    "poste": "Jungle",
    "description": "Dans les sombres failles de Runeterra, le démon nommé Evelynn cherche sa prochaine victime. Elle attire sa proie en se faisant passer pour une femme voluptueuse, et dès que sa cible succombe à ses charmes, Evelynn révèle sa véritable nature. Elle inflige alors à sa victime des tourments indicibles et se repaît de sa douleur. Pour un démon, ce ne sont que des plaisirs innocents. Aux yeux du reste de Runeterra, ce sont des contes terrifiants, qui rappellent les dangers du désir et de la luxure.",
    "competences": ["Ombre démoniaque", "Piques de haine", "Séduction", "Coup de fouet", "Faiseuse de veuves"],
    "skin": ["Evelynn ", "Evelynn Noire", "Evelynn Masquée", "Evelynn Tango", "Evelynn Perceuse de Coffres", "Evelynn Lune de Sang", "K/DA Evelynn", "K/DA Evelynn Edition Prestige", "Evelynn Folie Sucrée", "K/DA All Out Evelynn"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte24b4c6ec1beebb9/5db05fbddf78486c826dccf4/RiotX_ChampionList_evelynn.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Evelynn_0.jpg",
    "eb": 1350,
    "rp": 585

},{
    "nom": "Fiora",
    "surnom": "Sublime bretteuse",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Fiora est la duelliste la plus redoutée de Valoran : elle est connue pour ses manières brusques et sa vivacité d'esprit autant que pour la rapidité de sa rapière d'acier bleu. Née au sein de la Maison Laurent du royaume de Demacia, Fiora a pris le contrôle de la famille à son père dans les remous d'un scandale qui faillit la détruire. La réputation de la Maison Laurent a été ternie, mais Fiora investit toute son énergie à restaurer l'honneur de sa famille pour qu'elle reprenne sa place légitime parmi les grands de Demacia.",
    "competences": ["Danse de la duelliste", "Fente", "Riposte", "Botte secrète", "Défi suprême"],
    "skin": ["Fiora", "Fiora Garde Royal", "Fiora Oiseau Nocturne", "Proviseur Fiora", "Projet: Fiora", "Fiora Starlette des Plages", "Fiora à l'Epée Ascendante", "Fiora Percecoeur", "IG Fiora", "Fiora Pulsefire"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt935dd149b2617ac8/5db05fbcdc674266df3d5d36/RiotX_ChampionList_fiora.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Fiora_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Galio",
    "surnom": "Colosse",
    "classe": "Tank",
    "poste": "Milieu / Support",
    "description": "Près de la cité étincelante de Demacia, le colosse de pierre Galio monte une garde attentive. Érigé comme un rempart contre les mages ennemis, il reste souvent immobile pendant des décennies, jusqu'à ce que l'apparition de puissants flux magiques le ramène à la vie. Une fois éveillé, Galio profite de chaque seconde d'existence ; il s'enivre des frissons du combat et de l'honneur trop rare de défendre ses compatriotes. Mais ses triomphes ont toujours un arrière-goût amer, car la magie qu'il éradique est également la sève qui le fait vivre, et chaque victoire le renvoie au sommeil.",
    "competences": ["Frappe colossale", "Vents de guerre", "Bouclier de Durand", "Horion de la justice", "Entrée héroïque"],
    "skin": ["Galio", "Galio L'Enchanteur", "Galio Hextech", "Galio Commando", "Galio Cerbère", "Galio Séducteur", "Poulio", "Galio Infernal"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte41a162aed9339b7/5db05fc60b39e86c2f83dc0d/RiotX_ChampionList_garen.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Galio_0.jpg",
    "eb": 3150,
    "rp": 790

},{
    "nom": "Garen",
    "surnom": "Force de Demacia",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Garen est un guerrier fier et noble qui fait partie du Détachement hardi. Héritier des Crownguard, la famille chargée de défendre Demacia et ses idéaux, il est apprécié par ses compatriotes et respecté par ses ennemis. Équipé d'une armure résistante à la magie et d'une épée large, Garen affronte sans faillir les mages et les sorciers dans un véritable tourbillon d'acier.",
    "competences": ["Persévérance", "Coup décisif", "Courage", "Jugement", "Justice de Demacia"],
    "skin": ["Garen", "Garen Sanguin", "Garen Soldat du Désert", "Garen Commando", "Garen Chevalier d'Effroi", "Garen Vagabond", "Garen de la Légion D'Acier", "Garen Amiral Renégat", "Garen du Royaume en Guerre", "Dieu-Roi Garen", "Garen Demacia Vice", "Garen du Royaume des Mechas", "Garen du Royaume des Mechas Edition Prestige"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5890d1ab5c8be01f/5db05fc6823d426762825fe5/RiotX_ChampionList_galio.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Garen_0.jpg",
    "eb": 450,
    "rp": 260

},{
    "nom": "Gragas",
    "surnom": "Agitateur",
    "classe": "Combattant",
    "poste": "Jungle / Milieu",
    "description": "Aussi imposant que jovial, Gragas est un brasseur à l'impressionnante carrure qui cherche la recette de la bière parfaite. Venu d'on ne sait où, il continue de chercher les ingrédients les plus rares dans les terres vierges de Freljord, essayant toutes les variations de bière au cours de son périple. Souvent ivre et toujours impulsif, il est légendaire pour ses rixes, qui finissent souvent en fêtes nocturnes préjudiciables au mobilier urbain. Toute apparition de Gragas annonce beuverie et destruction. Dans cet ordre.",
    "competences": ["Tournée", "Fût roulant", "Rage d'ivrogne", "Coup de bidon", "Fût explosif"],
    "skin": ["Gragas", "Gragas Plongeur", "Gragas Péon", "Père Gragas", "Gragas la classe", "Gragas Vandale", "Gragas Oktoberfest", "Gragas Superfan", "Fnatic Gragas", "Gragas Brasseur Solaire", "Gragas Arctique", "Gragas Purificateur"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt795841adba722f85/5db05fc43a326d6df6c0e7b9/RiotX_ChampionList_gragas.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Gragas_0.jpg",
    "eb": 3150,
    "rp": 790

},{
    "nom": "Hecarim",
    "surnom": "Ombre de la guerre",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Fusion spectrale de l'homme et de la bête, Hecarim est condamné à pourchasser les âmes des vivants pour l'éternité. Lorsque les ombres ont envahi les Îles bénies, ce fier chevalier et sa horde de cavaliers ont été anéantis par les énergies destructrices de la Ruine. À présent, quand la Brume noire s'étend sur Runeterra, il mène la charge et se plaît à massacrer et écraser ses ennemis sous ses lourds sabots.",
    "competences": ["Galop", "Carnage", "Essence de la peur", "Charge dévastatrice", "Légion des ombres"],
    "skin": ["Hecarim", "Hecarim Chevalier de Sang", "Hecarim Faucheur", "Hecarim Sans Tête", "Hecarim Arcade", "Hecarim Sylvestre", "Hecarim Brise-Monde", "Hecarim Lancier Zéro", "Hecarim de L'Ouest", "Hecarim Cavalier Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blteb9ce5304ec48e19/5db05fc5df78486c826dccfa/RiotX_ChampionList_hecarim.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Hecarim_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Illaoi",
    "surnom": "Prêtresse du kraken",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "La stature colossale d'Illaoi n'a d'égale que sa foi implacable. Prophétesse du Grand Kraken, elle se sert d'une énorme idole dorée pour arracher les esprits de ses ennemis et faire voler en éclats leur perception de la réalité. Ceux qui défient « l'Apôtre de la vérité de Nagakabouros » découvrent bien vite qu'Illaoi ne combat jamais seule, car le dieu des Îles aux serpents lui prête main-forte.",
    "competences": ["Prophétesse d'un dieu ancestral", "Coup de tentacule", "Âpre leçon", "Épreuve de l'esprit", "Acte de foi"],
    "skin": ["Illaoi", "Illaoi Messagère du Néant", "Illaoi Résistante", "Illaoi Invocatrice"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltc44e90a5547120a2/5db05fc5347d1c6baa57be2b/RiotX_ChampionList_illaoi.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Illaoi_0.jpg",
    "eb": 6300,
    "rp": 975

},{
    "nom": "Irelia",
    "surnom": "Danseuse des lames",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Des nombreux héros qui naquirent de l'occupation noxienne, aucun n'est plus étonnant que la jeune Irelia de Navori. Formée aux danses ancestrales de sa province, elle adapta son art pour la guerre, se servant de ses mouvements gracieux et précis pour faire léviter une multitude de lames acérées. Après avoir prouvé ses talents de guerrière, elle devint le chef et le symbole de la Résistance ; à ce jour, sa tâche est toujours de défendre son pays.",
    "competences": ["Ferveur ionienne", "Rush fatal", "Danse de défi", "Duo parfait", "Pointe de l'avant-garde"],
    "skin": ["Irelia", "Irelia Assassin", "Irelia Aviatrice", "Cyber Irelia", "Irelia Lame de Glace", "Irelia de l'Ordre du Lotus", "Irelia à L'Epée Divine", "IG Irelia", "Projet: Irelia", "Projet: Irelia", "Projet: Irelia Edition Prestige", "Irelia de L'Ouest"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltf5f2b8de93870aef/5db05fcea6470d6ab91ce59a/RiotX_ChampionList_irelia.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Irelia_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Jarvan IV",
    "surnom": "Exemple demacien",
    "classe": "Tank",
    "poste": "Jungle",
    "description": "Le prince Jarvan, héritier de la dynastie des Lightshield, doit un jour monter sur le trône de Demacia. Élevé de manière à incarner les plus hautes vertus de sa nation, il est contraint de trouver un équilibre entre les attentes placées en lui et son désir de combattre sur la ligne de front. Jarvan inspire ses troupes avec son courage, sa détermination et son dévouement. Il porte haut les couleurs de sa famille et révèle chaque jour davantage sa véritable force de prochain souverain.",
    "competences": ["Cadence martiale", "Frappe du dragon", "Égide dorée", "Étendard demacien", "Cataclysme"],
    "skin": ["Jarvan IV", "Jarvan IV Commando", "Jarvan IV Tueur de Dragons", "Jarvan IV Ténébreux", "Jarvan IV Héros de Guerre", "Jarvan IV du Royaume en Guerre", "Fnatic Jarvan IV", "Jarvan IV du Pulsar Sombre", "SSG Jarvan IV", "Jarvan IV Hextech", "Jarvan IV UV"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt5898626d7016d222/5db05fcfdc674266df3d5d3c/RiotX_ChampionList_jarvaniv.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/JarvanIV_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Jax",
    "surnom": "Maître d'armes",
    "classe": "Combattant",
    "poste": "Jungle / Haut",
    "description": "Jax, dont nul ne peut égaler l'art du sarcasme et des armements inhabituels, est connu comme le dernier maître d'armes d'Icathia. Après la chute de sa terre natale, ravagée par le Néant qu'elle avait eu l'orgueil de déchaîner, Jax et ses compagnons jurèrent de protéger le peu qu'il en restait. La magie montant de nouveau dans le monde, la menace se fait plus pressante et Jax parcourt Valoran, porteur des dernières lumières d'Icathia. Il éprouve les qualités de tous les guerriers qu'il rencontre pour voir s'ils sont assez puissants pour combattre à ses côtés...",
    "competences": ["Acharnement", "Frappe bondissante", "Élargissement", "Contre-attaque", "Force du maître"],
    "skin": ["Jax", "Le Grand Jax", "Jax le Vandale", "Jax le Pêcheur", "Jax Pax", "Jaximus", "Jax Shaolin", "Jax Némésis", "SKT T1 Jax", "Jax Purificateur", "Jax au Bâton Divin", "Jax du Royaume des Mechas"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt6b301613598c3f17/5db05fcf89fb926b491ed81d/RiotX_ChampionList_jax.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Jax_0.jpg",
    "eb": 1350,
    "rp": 585

},{
    "nom": "Kayn",
    "surnom": "Faucheur de l'ombre",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Expert inégalé dans la pratique de la magie des ombres, Shieda Kayn combat pour accomplir sa véritable destinée : mener un jour l'Ordre de l'ombre vers une ère nouvelle où Ionia régnera en maître. Il manie Rhaast, une arme darkin douée de raison, sans jamais craindre la corruption de son corps et de son esprit. Il ne peut y avoir pour lui que deux fins possibles : soit Kayn pliera l'arme à sa volonté, soit la faux maléfique le consumera complètement, ouvrant la voie à la destruction de tout Runeterra.",
    "competences": ["Faux des Darkin", "Moisson cruelle", "Entaille sombre", "Passe-muraille", "Intrusion obscure"],
    "skin": ["Kayn", "Kayn Chasseur D'Ames", "Kayn de L'Odyssée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt64edf2d3729b57c1/5db05fd656458c6b3fc17519/RiotX_ChampionList_kayn.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kayn_0.jpg",
    "eb": 6300,
    "rp": 975

},{
    "nom": "Kha'Zix",
    "surnom": "Faucheur du Néant",
    "classe": "Assassin",
    "poste": "Jungle",
    "description": "Le Néant grandit et le Néant s'adapte : parmi ses nombreuses engeances, nul n'incarne mieux cette vérité que Kha'Zix. Ce monstre est né pour survivre et pour tuer les plus forts. Quand il ne parvient pas à ses fins, il évolue et se crée de nouveaux moyens plus efficaces d'abattre sa proie. Alors que Kha'Zix n'était à l'origine qu'une créature sans cervelle, son intelligence s'est développée autant que sa forme physique. Aujourd'hui, chacune de ses traques est planifiée et profite même de la peur viscérale qu'il instille dans le cœur de ses victimes.",
    "competences": ["Menace invisible", "Goût de la peur", "Pique du Néant", "Bond", "Assaut du Néant"],
    "skin": ["Kha'Zix", "Mecha Kha'Zix", "Kha'Zix Gardien des Sables", "Kha'Zix du Lotus Mortel", "Kha'Zix du Pulsar Sombre", "Kha'Zix du Championnat", "Kha'Zix de L'Odyssée"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt363d7d62846ffc87/5db05fd6e9effa6ba5295f9f/RiotX_ChampionList_khazix.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Khazix_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Kled",
    "surnom": "Cavalier colérique",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Guerrier aussi intrépide que grincheux, Kled est la plus pure incarnation de la fureur de Noxus. C'est un symbole que les soldats de l'empire adorent, dont les officiers se méfient et que la noblesse déteste. Beaucoup affirment que Kled a participé à toutes les campagnes des légions, qu'il a obtenu toutes les distinctions militaires et qu'il n'a jamais reculé devant un combat. Si la véracité des détails est souvent douteuse, une partie de sa légende reste indéniable : quand il charge sur le dos de Skaarl, sa monture un peu froussarde, Kled se bat pour protéger ce qui lui appartient... et s'emparer de tout le reste !",
    "competences": ["Skaarl, le Lézard froussard", "Piège à ours en laisse", "Penchant pour la violence", "Joute", "Chaaaaaaaargez !!!"],
    "skin": ["Kled", "Sir Kled", "Comte Kledula", "Kled Maraudeur"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt1670a0dd8fd5edca/5db05fd66e8b0c6d038c5ca8/RiotX_ChampionList_kled.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kled_0.jpg",
    "eb": 6300,
    "rp": 975

},{
    "nom": "Lee Sin",
    "surnom": "Moine aveugle",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Maître des antiques arts martiaux d'Ionia, Lee Sin est un combattant dévoué à de nobles principes qui canalise l'esprit du dragon pour affronter tous les défis. Bien qu'il ait perdu la vue il y a bien des années, le moine-guerrier a dédié sa vie à la protection de sa terre natale contre tous ceux qui voudraient en déstabiliser l'équilibre spirituel. Les ennemis qui sous-estiment son apparence méditative auront peu de temps pour le regretter, face à ses poings de feu et à ses coups de pied flamboyants.",
    "competences": ["Rafale", "Onde sonore/Coup résonant", "Rempart/Force d'âme", "Trombe/Fracture", "Rage du dragon"],
    "skin": ["Lee Sin", "Lee Sin Classique", "Lee Sin Acolyte", "Lee Sin Poing du Dragon", "Lee Sin Muay Thaï", "Lee PiSin", "SKT T1 Lee Sin", "Lee Sin Pugiliste", "Lee Sin Poing Divin", "Lee Sin Meneur de Jeu", "Lee Sin Héraut de la Nuit", "Lee Sin Héraut de la Nuit Edition Prestige", "Lee Sin Dragon des Tempêtes"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt075d278529811445/5db05fde6af83b6d7032c8fe/RiotX_ChampionList_leesin.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/LeeSin_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Leona",
    "surnom": "Aube radieuse",
    "classe": "Tank",
    "poste": "Support",
    "description": "Imprégnée de l'énergie du soleil, Leona est une guerrière sainte des Solaris qui défend le Mont Targon ; elle combat avec sa Lame du zénith et son Bouclier de l'aube. Sa peau brûle d'éclats ardents et ses yeux crépitent au rythme de la Manifestation céleste qui l'habite. Équipée d'une armure d'or et affligée par le terrible fardeau de connaissances antiques, Leona apporte à certains l'illumination, à d'autres la mort.",
    "competences": ["Rayon de soleil", "Bouclier de l'aube", "Éclipse", "Lame du zénith", "Éruption solaire"],
    "skin": ["Leona", "Leona Valkyrie", "Leona Leona Protectrice", "Leona de L'Iron Solari", "Leona Bain de Soleil", "Projet: Leona", "Leona Reine du Grill", "Leona de L'Eclipse Solaire", "Leona de L'Eclipse Lunaire", "Leona du Royaume des Mechas"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt8d46ada03ea1da8c/5db05fdf347d1c6baa57be31/RiotX_ChampionList_leona.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Leona_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Maître Yi",
    "surnom": "Fine lame Wuju",
    "classe": "Assassin",
    "poste": "Jungle",
    "description": "Maître Yi a renforcé son corps et affûté son esprit jusqu'à ce que réflexion et action ne fassent plus qu'un. Bien qu'il ne fasse appel à la violence qu'en dernier recours, la grâce et la vitesse avec lesquelles il manie son épée assurent une résolution rapide de tout conflit. En tant que l'un des derniers praticiens vivants de l'art martial ionien appelé Wuju, Yi a voué sa vie à perpétuer l'héritage de son peuple, scrutant les nouveaux disciples potentiels à l'aide des Sept lentilles d'éveil pour identifier les plus dignes de recevoir son enseignement.",
    "competences": ["Coup double", "Assaut éclair", "Méditation", "Style Wuju", "Highlander"],
    "skin": ["Maître Yi", "Maître Yi L'Assassin", "Maître Yi L'Elu", "Maître Yi De Ionia", "Samouraï Yi", "Maître Yi Chasseur de Têtes", "Projet: Yi", "Maître Yi Lame Cosmique", "Yi à L'Epée Eternelle", "Bonhomme de Neige Yi", "Maître Yi lune de Sang", "Maître Yi Psychoguerrier"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt46e861d7e6c0ab0c/5db05fe8df78486c826dcd12/RiotX_ChampionList_masteryi.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/MasterYi_0.jpg",
    "eb": 450,
    "rp": 260

},{
    "nom": "Malphite",
    "surnom": "Éclat du monolithe",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Massive créature de pierre vivante, Malphite lutte pour imposer un ordre sacré dans un monde chaotique. Né pour servir l'obélisque mystique que l'on nomme le Monolithe, il utilisa son incroyable force élémentaire pour protéger son créateur, mais n'y parvint pas. Seul survivant de la destruction qui s'ensuivit, Malphite doit désormais supporter les inconséquences des êtres de chair et trouver un nouveau rôle, digne du dernier survivant de son espèce.",
    "competences": ["Bouclier de granit", "Éclat sismique", "Coup de tonnerre", "Choc au sol", "Force indomptable"],
    "skin": ["Malphite", "Malphite aux Trèfles", "Malphite Corallien", "Malphite de Marbre", "Malphite D'Obsidienne", "Malphite Glacial", "Mecha Malphite", "Malphite Flibustier", "Malphite de L'Odyssée", "Malphite du Pulsar Sombre", "Malphite du Pulsar Sombre Edition Prestige", "FPX Malphite"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt4d3b4a7e4c44ced7/5db05fdeadc8656c7d24e7e2/RiotX_ChampionList_malaphite.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Malphite_0.jpg",
    "eb": 1350,
    "rp": 585

},{
    "nom": "Maokai",
    "surnom": "Tréant torturé",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Maokai est un immense tréant possédé par la colère, qui combat les horreurs impies des Îles obscures. Il est devenu l'incarnation de la vengeance après la destruction de ses terres par un cataclysme magique, n'échappant lui-même à l'état de mort-vivant que grâce à l'eau de la vie qui l'irrigue. Autrefois, Maokai était un paisible esprit de la nature ; aujourd'hui, il combat avec fureur le fléau mort-vivant qui accable les Îles obscures pour rendre à l'archipel son ancienne beauté.",
    "competences": [ "Sève magique", "Coup de ronces", "Croissance torturée", "Jet d'arbrisseau", "Emprise de la nature"],
    "skin": ["Maokai", "Maokai Calciné", "Maokai Totémique", "Maokai Festif", "Maokai Hanté", "Maokai Gardien de But", "Miaoukai", "Maokai Héros de Guerre", "Maokai Brise-Monde"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt757d0e8855d51e03/5db05fe72140ec675d68f4a1/RiotX_ChampionList_maokai.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Maokai_0.jpg",
    "eb": 4800,
    "rp": 880

},{
    "nom": "Mordekaiser",
    "surnom": "Revenant de fer",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Deux fois abattu et trois fois né, Mordekaiser est un chef de guerre brutal venu d'un lointain passé. Il utilise ses pouvoirs nécromantiques pour lier les âmes à une éternité de servitude. Il n'y a plus guère de gens pour se souvenir de ses anciennes conquêtes ou pour connaître l'étendue de ses vrais pouvoirs. Pourtant, certaines âmes antiques en ont gardé la mémoire et craignent le jour où il prétendra de nouveau régner sur les vivants et les morts.",
    "competences": ["Vortex de ténèbres", "Oblitération", "Indestructible", "Emprise funeste", "Royaume des morts"],
    "skin": ["Mordekaiser", "Mordekaiser le Dragonnier", "Mordekaiser infernal", "Mordekaiser Pentakill", "Lord Mordekaiser", "Mordekaiser Roi de Trèfle", "Mordekaiser du Pulsar Sombre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt2392a60ff2a2d726/5db05fe8242f426df132f96d/RiotX_ChampionList_mordekaiser.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Mordekaiser_0.jpg",
    "eb": 1350,
    "rp": 585

},{
    "nom": "Zac",
    "surnom": "Arme secrète",
    "classe": "Tank",
    "poste": "Jungle",
    "description": "Zac est le produit d'une fuite toxique qui, dégoulinant par une veine techno-chimique, se transforma en bassin dans une profonde caverne du Puisard de Zaun. En dépit de ses humbles origines, Zac a évolué pour passer du stade de dépôt primordial à celui d'être pensant qui se faufile dans les canalisations de la ville. De temps en temps, il émerge pour aider ceux qui ne s'en sortent pas seuls ou pour reconstruire les infrastructures en mauvais état de Zaun.",
    "competences": [ "Division cellulaire", "Étirements", "Matière instable", "Fronde", "Boing !"],
    "skin": ["Zac", "Zac Arme Spéciale", "Zaquatique", "SKT T1 Zac", "Proto Zac"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt9412083c2b98b9c8/5db0601d6af83b6d7032c910/RiotX_ChampionList_zac.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Zac_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Yorick",
    "surnom": "Berger des âmes",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Yorick est le dernier survivant d'un ordre religieux disparu depuis longtemps et son pouvoir sur les morts est à la fois une bénédiction et une malédiction. Prisonnier des Îles obscures, il n'a pour compagnons que les cadavres pourrissants et les esprits hurlants qu'il rassemble. Les actes monstrueux de Yorick trahissent son noble objectif : libérer sa terre natale de la malédiction de la Ruine. ",
    "competences": [ "Berger des âmes", "Derniers sacrements", "Derniers sacrements", "Brume endeuillée", "Élégie des Îles"],
    "skin": ["Yorick", "Yorick Ensevelisseur", "Yorick Pentakill", "Yorick a la Pelle Pure", "Miourick", "Yorick Résistant", "Yorick Résistant"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt570d89dd9a76ba08/5db0601c823d426762825ff9/RiotX_ChampionList_yorick.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yorick_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Yone",
    "surnom": "Inoublié",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "Au cours de sa vie, Yone fut le demi-frère de Yasuo et un disciple renommé de l'école d'armes de son village. Mais après avoir été tué par son propre frère, il se retrouva hanté par une entité malveillante dans le royaume spirituel. Il dut la tuer avec sa propre épée. Désormais maudit et forcé de porter le masque de l'azakana, Yone chasse inlassablement ces créatures démoniaques afin de comprendre ce qu'il est devenu. ",
    "competences": ["Voie du chasseur", "Acier mortel", "Fendoir spirituel", "Libération spirituelle", "Destin scellé"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yone_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yone_0.jpg",
    "skin": ["Yone", "Yone Fleur Spirituelle"],
    "eb": 6300,
    "rp": 975
},{
    "nom": "Yasuo",
    "surnom": "Disgracié",
    "classe": "Combattant",
    "poste": "Milieu",
    "description": "L'Ionien Yasuo est un épéiste agile et résolu, capable de déchaîner contre ses ennemis la puissance de l'air. Alors qu'il était encore un jeune homme fier, il fut accusé à tort d'avoir assassiné son maître. Incapable de prouver son innocence, il dut fuir et fut finalement contraint de tuer son frère pour se défendre. Même après la découverte du véritable meurtrier de son maître, Yasuo ne parvint pas à faire la paix avec sa conscience. Il erre désormais dans son pays natal, et seul le vent guide sa lame. ",
    "competences": ["Voie du vagabond", "Tempête d'acier", "Mur de vent", "Cercle tranchant", "Dernier soupir"],
    "skin": ["Yasuo", "Yasuo de l'Ouest", "Projet : Yasuo", "Yasuo Lune de Sang", "Yasuo Héraut de la Nuit", "Yasuo de l'Odysée", "Yasuo Boss de Combat", "True Damage Yasuo", "True Damage Yasuo Edition Prestige", "Yasuo Fleur Spirituelle"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3a319fc884dc6880/5db0601c242f426df132f985/RiotX_ChampionList_yasuo.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Yasuo_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Xin Zhao",
    "surnom": "Sénéchal de Demacia",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Xin Zhao est un guerrier résolu qui sert fidèlement la dynastie régnante des Lightshield. Naguère condamné à se battre dans les arènes de combat de Noxus, il survécut à d'innombrables rencontres de gladiateurs. Mais après avoir été libéré par les forces de Demacia, il jura allégeance à ses braves libérateurs. Armé de sa lance à trois pointes, Xin Zhao combat désormais pour son royaume, défiant ses ennemis avec audace, quelles que soient ses chances de succès. ",
    "competences": ["Détermination", "Frappe des trois serres", "Vent et foudre", "Charge audacieuse", "Garde circulaire"],
    "skin": ["Xin Zhao", "Xin Zhao Commando", "Xin Zhao Impérial", "Xin Zhao Viscero", "Xin Zhao Hussar Ailé", "Xin Zhao du Royaume en Guerre", "Xin Zhao Agent Secret", "Xin Zhao Tueur de Dragons", "Xin Zhao Défenseur Cosmique", "Xin Zhao Maraudeur"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltca4486a1630ef517/5db0601ce9d7526ab429e54a/RiotX_ChampionList_xinzhao.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/XinZhao_0.jpg",
    "eb": 1350,
    "rp": 585
},{
    "nom": "Wukong",
    "surnom": "Roi des singes",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Wukong est un Vastaya qui utilise sa force, son agilité et son intelligence pour semer la confusion parmi ses adversaires et prendre sur eux l'avantage. Après s'être fait un ami en la personne d'un guerrier nommé Maître Yi, Wukong est devenu le dernier tenant d'un art martial antique connu sous le nom de Wuju. Armé de son bâton enchanté, Wukong veut empêcher Ionia de devenir ruines et décombres. ",
    "competences": ["Peau de pierre", "Écrasement", "Guerrier espiègle", "Nimbus", "Cyclone"],
    "skin": ["Wukong", "Wukong Volcanique", " Général Wukong", "Wukong Dragon de Jade", "Wukong des Enfers", "Wukong Radieux", "Wukong Lancier Stratus"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltaf44673e1a16af30/5db05fe87d28b76cfe439807/RiotX_ChampionList_monkeyking.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/MonkeyKing_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Warwick",
    "surnom": "Fureur déchaînée de Zaun",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Warwick est un monstre qui chasse dans les rues grisâtres de Zaun. Transformé par des expériences horribles, son corps est désormais muni d'un système intriqué de chambres et de pompes qui injecte une rage alchimique dans ses veines. Surgissant des ombres, il traque les criminels qui terrorisent les profondeurs de la ville. Warwick est attiré par le sang, dont l'odeur le rend fou. Si sa proie saigne, elle n'a aucune chance de lui échapper. ",
    "competences": ["Soif inextinguible", "Dents de la bête", "Traque sanguinaire", "Hurlement bestial", "Contrainte infinie"],
    "skin": ["Warwick", "Warwick Gris", "Urf le Lamantin", "Grand Méchant Warwick", "Warwick de la Toundra", "Warwick Sauvage", "Warwick de Feu", "Warwick Hyène", "Warwick Maraudeur", "Urfwick", "Warwick Gardien Lunaire", "Projet : Warwick"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt08a92f3897cfc8f5/5db0601ddc674266df3d5d5c/RiotX_ChampionList_warwick.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Warwick_0.jpg",
    "eb": 450,
    "rp": 260
},{
    "nom": "Volibear",
    "surnom": "Tempête impitoyable",
    "classe": "Combattant",
    "poste": "Jungle / Haut",
    "description": "Pour ceux qui le vénèrent encore, Volibear est l'incarnation de la tempête. Destructeur, sauvage et déterminé, il existait bien avant que les mortels n'arpentent la toundra de Freljord ; désormais, il défend férocement les terres qu'il a créées avec sa famille de demi-dieux. Habité d'une haine profonde envers la civilisation et la faiblesse qu'elle a engendrée, il se bat à coups de crocs et de griffes pour restaurer les traditions d'autrefois, celles du temps où la terre était sauvage et où le sang coulait librement. ",
    "competences": [ "Tempête impitoyable", "Coup fulgurant", "Folie mutilatrice", "Foudroiement", "Tempête incarnée"],
    "skin": ["Volibear", "Fulguro Volibear", "Volibear Nordique", "Volibear Runique", "Capitaine Volibear", "Volibear El Rayo", "Volibear aux Mille Piques"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Volibear_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Volibear_0.jpg",
    "eb": 3150,
    "rp": 790
},{
    "nom": "Vi",
    "surnom": "Cogne de Piltover",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Autrefois criminelle des quartiers louches de Zaun, Vi est une femme sans peur, mais pas sans reproche, au tempérament bouillant, impulsive, qui n'a qu'un respect mesuré pour les représentants de l'autorité. Vi a grandi seule et a développé d'excellents instincts de survie ainsi qu'un sens de l'humour assez caustique. Aujourd'hui, elle travaille avec les gendarmes de Piltover pour maintenir l'ordre et se sert pour cela de puissants gantelets Hextech qui n'ont peur de frapper ni les plus épais murs de briques ni les suspects. ",
    "competences": [ "Bouclier antichoc", "Brise-coffre", "Coups fracassants", "Force excessive", "Marteau-pilon"],
    "skin": ["Vi", "Vi de l'Unité Néon", "Agent Vi", "Vi Séductrice", "Vi Démoniaque", "Vi du Royaume en Guerre", "Projet : Vi", "Vi Bourreau des coeurs", "Vi Psychoguerrière"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3bd79abf330fc807/5db06014dc674266df3d5d56/RiotX_ChampionList_vi.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Vi_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Urgot",
    "surnom": "Broyeur",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Autrefois, Urgot était un puissant bourreau noxien. Il fut trahi par l'empire pour lequel il avait tant tué. Ceint de chaînes de fer, il fut contraint d'apprendre le vrai sens de la force dans les Oubliettes, une mine de forçats située dans les profondeurs de Zaun. Il put émerger à l'occasion d'un désastre qui jeta toute la ville dans le chaos. À présent, son ombre imposante s'étend dans son empire criminel souterrain. Levant sur ses victimes les chaînes qui le maintenaient naguère en esclavage, il veut purger son nouveau foyer, nouvelle croisée des douleurs, de tous ceux qu'il en estime indignes. ",
    "competences": [ "Flammes purificatrices", "Torpille corrosive", "Géhenne", "Mépris", "Règne de la terreur"],
    "skin": ["Urgot", "Cragot Ennemi Géant", "Urgot le Boucher", "Proto Urgot", "Urgot de l'Ouest", "Urgot Cosplay Gardien des Pyjamas"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt162b78ba3ece1d4f/5db060157d28b76cfe43981b/RiotX_ChampionList_urgot.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Urgot_0.jpg",
    "eb": 3150,
    "rp": 790
},{
    "nom": "Udyr",
    "surnom": "Gardien des esprits",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Plus qu'un simple mortel, Udyr est le réceptacle de la puissance sauvage de quatre esprits animaux primitifs. Quand Udyr cherche en lui les esprits féroces de la nature, il libère leur force exceptionnelle : la vitesse et la férocité du tigre, la résistance de la tortue, la force de l'ours et la flamme éternelle du phénix. En unissant ces esprits, Udyr est capable de défaire quiconque s'opposerait aux forces de la nature. ",
    "competences": ["Agilité du singe", "Posture du tigre", "Posture de la tortue", "Posture de l'ours", "Posture du phénix"],
    "skin": ["", "Udyr", "Udyr Ceinture Noire", "Udyr Primitif", "Udyr Gardien des Esprits", "Udyr Incognito", "Udyr Oracle Draconique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt8350eda62a9ed56c/5db060150b39e86c2f83dc2b/RiotX_ChampionList_udyr.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Udyr_0.jpg",
    "eb": 1350,
    "rp": 585
},{
    "nom": "Tryndamere",
    "surnom": "Roi barbare",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Poussé par une rage infinie, Tryndamere traverse Freljord et parfait sa maîtrise du combat en défiant les plus grands guerriers du nord, pour se préparer aux jours sombres qui s'annoncent. Longtemps dominé par sa haine, il cherchait à se venger de l'annihilation de son clan, mais il a depuis rencontré Ashe et trouvé un nouveau foyer auprès des Avarosans. Sa force est pratiquement surhumaine et son courage légendaire, et cela lui a permis de remporter avec ses nouveaux alliés d'innombrables victoires plus qu'improbables. ",
    "competences": ["Vague de violence", "Soif de sang", "Moquerie", "Balafre", "Rage inépuisable"],
    "skin": ["Tryndamere", "Tryndamere des Highlands", "Roi Tryndamere", "Tryndamere Viking", "Tryndamere Possédé", "Tryndamere Sultan", "Tryndamere du Royaume en Guerre", "Tryndamere du Cauchemar", "Tryndamere Chasseur de Bêtes", "Tryndamere Techno-Chimique", "Tryndamere Lune de Sang"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3b217144ed3a7faa/5db08d643a326d6df6c0e907/RiotX_ChampionList_tryndamere.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Tryndamere_0.jpg",
    "eb": 1350,
    "rp": 585
},{
    "nom": "Trundle",
    "surnom": "Roi des trolls",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Trundle est un troll massif, sournois et malicieux. Il n'y a rien qu'il ne puisse soumettre à sa volonté avec son gourdin, pas même Freljord. Extrêmement territorial, Trundle traque tous ceux qui sont assez fous pour pénétrer sur ses terres. Ensuite, armé de son imposante massue en glace pure, il fait trembler ses ennemis avant de leur planter des éclats de glace dans le corps et de se moquer d'eux tandis que la toundra absorbe leur sang. ",
    "competences": ["Tribut du roi", "Morsure", "Royaume gelé", "Montagne de glace", "Soumission"],
    "skin": ["Trundle", "Trundle Batteur", "Trundle Féreuilleur", "Trundle Classique", "Constable Trundle", "Trundle Brise-Monde", "Trundle Tueur de Dragons"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt05eb0518bbe963c0/5db0600ba6470d6ab91ce5b2/RiotX_ChampionList_trundle.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Trundle_0.jpg",
    "eb": 3150,
    "rp": 790
},{
    "nom": "Taric",
    "surnom": "Bouclier de Valoran",
    "classe": "Support",
    "poste": "Support",
    "description": "Taric est la Manifestation du Protecteur, doté d'un incroyable pouvoir en tant que gardien de la vie, de l'amour et de la beauté sur Runeterra. Couvert d'opprobre pour avoir abandonné son devoir et exilé de sa terre natale de Demacia, Taric a entrepris l'ascension du Mont Targon en quête de rédemption et s'est découvert une mission supérieure au sein des étoiles. Imprégné de la force antique de Targon, le Bouclier de Valoran monte désormais constamment la garde contre la corruption insidieuse du Néant. ",
    "competences": ["Plastronneur", "Bénédiction stellaire", "Bastion", "Éblouissement", "Lumière cosmique"],
    "skin": ["Taric", "Taric Emeraude", "Taric du Cinquième Âge", "Taric Hémolithe", "Taric à malibu", "Taric Clair-Pavois"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt6b2d37773c3621e1/5db0600d347d1c6baa57be55/RiotX_ChampionList_taric.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Taric_0.jpg",
    "eb": 1350,
    "rp": 585
},{
    "nom": "Tahm Kench",
    "surnom": "Roi des rivières",
    "classe": "Support",
    "poste": "Haut / Support",
    "description": "Au fil du temps, on lui a donné de nombreux noms. Le démon Tahm Kench voyage dans les cours d'eau de Runeterra, nourrissant son insatiable appétit de la misère des autres. Bien qu'il puisse sembler particulièrement charmant et fier, il rôde dans le monde tel un vagabond à la recherche d'une proie sans méfiance. Sa langue extensible peut étourdir un guerrier en armure à plusieurs mètres de distance, avant de l'attirer dans son estomac gargouillant vers un abysse dont il n'a que peu de chances de revenir. ",
    "competences": ["Goût acquis", "Coup de langue", "Dévoration", "Peau épaisse", "Traversée des abysses"],
    "skin": ["Tahm Kench", "Tahm Kench Cordon-Bleu", "Urf Tahm Kench", "Tahm Kench Numismate"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt87be9cf38f3bc986/5db0600c56458c6b3fc17533/RiotX_ChampionList_tahmkench.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/TahmKench_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Skarner",
    "surnom": "Gardien de cristal",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Skarner est un immense scorpion cristallin venu d'une vallée cachée de Shurima. Issus de l'ancienne race des Brackerns, Skarner et les siens sont connus pour leur grande sagesse et leurs liens profonds avec la terre : leur âme a fusionné avec de puissants cristaux de vie qui contiennent les pensées et les souvenirs de leurs ancêtres. À une époque si reculée que nul ne s'en souvient, les Brackerns sont entrés en hibernation pour échapper à la destruction magique, mais des événements menaçants viennent de réveiller Skarner. Seul Brackern éveillé, il lutte pour protéger les siens contre ceux qui cherchent à les blesser. ",
    "competences": ["Flèches cristallines", "Taillade de cristal", "Exosquelette cristallin", "Brèche", "Empalement"],
    "skin": ["Skarner", "Skarner Fléau des Sables", "Skarner Tellurique", "Proto Skarner Alpha", "Skarner Gardien des Sables", "Skarner Dard Cosmique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt842a8149ba8b7a3d/5db06003bd24496c390ae4f6/RiotX_ChampionList_skarner.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Skarner_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Sion",
    "surnom": "Colosse mort-vivant",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Guerrier sanguinaire d'une époque révolue, Sion était célébré comme un héros à Noxus, pour avoir étouffé un roi de Demacia de ses mains nues. Il était l'objet d'une telle dévotion qu'on lui refusa le repos éternel, en le ramenant à la vie pour continuer à servir son empire. Il massacre sans distinction alliés comme ennemis sur son chemin, prouvant à cette occasion qu'il n'a plus rien d'humain. Son armure rivetée à même la chair, Sion charge sans relâche au combat, luttant entre deux coups de sa puissante hache pour tenter de se remémorer qui il était. ",
    "competences": ["Gloire posthume", "Fracas meurtrier", "Feu intérieur", "Cri du tueur", "Assaut inarrêtable"],
    "skin": ["Sion", "Sion Hextech", "Sion Barbare", "Sion Bûcheron", "Sion Massacreur", "Mecha Sion Zéro", "Sion Brise-Monde"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt7f28f1d689e4ad3d/5db06004adc8656c7d24e802/RiotX_ChampionList_sion.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sion_0.jpg",
    "eb": 1350,
    "rp": 585
},{
    "nom": "Shyvana",
    "surnom": "Demi-dragon",
    "classe": "Combattant",
    "poste": "Jungle / Haut",
    "description": "Shyvana est une créature dont le cœur palpite au rythme brûlant d'un éclat de rune magique. Bien qu'elle paraisse souvent humanoïde, elle peut se transformer à volonté en un terrifiant dragon, crachant des flammes sur ses ennemis. Après avoir sauvé la vie du prince héritier Jarvan IV, Shyvana sert au sein de sa garde royale, luttant pour être acceptée par le peuple suspicieux de Demacia. ",
    "competences": ["Fureur d'enfant de dragon", "Morsure double", "Combustion", "Souffle de flammes", "Vol du dragon"],
    "skin": ["Shyvana", "Shyvana Ecailles de Fer", "Shyvana Chamane", "Shyvana aux Flammes Noires", "Shyvana Dragon de Glace", "Shyvana du Championnat", "Super Shyvana Intergalactique"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltdb320ec49e0e02a7/5db0600456458c6b3fc1752d/RiotX_ChampionList_shyvana.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Shyvana_0.jpg",
    "eb": 3150,
    "rp": 790
},{
    "nom": "Shen",
    "surnom": "Oeil du crépuscule",
    "classe": "Tank",
    "poste": "Haut / Support",
    "description": "Shen, l'Oeil du crépuscule, est le chef d'un groupe secret de guerriers ioniens connu sous le nom de Kinkou. Désireux d'échapper aux méfaits des émotions, des préjugés et de l'ego, il porte un regard désintéressé sur les choses en arpentant le chemin invisible entre le royaume des esprits et le monde physique. Ayant pour mission d'assurer leur équilibre, Shen emploie ses lames d'acier et son énergie arcanique contre ceux qui voudraient le menacer. ",
    "competences": ["Barrière de ki", "Assaut crépusculaire", "Refuge spirituel", "Rush des ombres", "Soutien indéfectible"],
    "skin": ["Shen", "Shen de Glace", "Shen au Manteau Jaune", "Shen Chirurgien", "Shen Lune de Sang", "Samouraï Shen", "TPA Shen", "Shen Pulsefire", "Shen Infernal", "Shen Psychoguerrier"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltd142d174831b78e9/5db06004242f426df132f97f/RiotX_ChampionList_shen.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Shen_0.jpg",
    "eb": 3150,
    "rp": 790
},{
    "nom": "Sett",
    "surnom": "Patron",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Sett s'est imposé en tant que chef du monde criminel florissant d'Ionia après la guerre contre Noxus. Bien qu'il ait fait ses débuts de manière modeste dans les arènes de combat de Navori, il s'est rapidement fait une réputation pour sa force sauvage et sa capacité à encaisser tous les coups sans broncher. Après s'être battu pour se faire une place parmi les combattants locaux, Sett s'est hissé au sommet à la force de ses poings pour finalement prendre le contrôle des arènes dans lesquelles il se battait autrefois. ",
    "competences": ["Roi de l'arène", "Rogne", "Coup cathartique", "Casse-tête", "Le Clou du spectacle"],
    "skin": ["Sett", "Sett du Royaume des Mechas", "Sett Dragon Obsidienne", "Sett Dragon Obsedienne Edition Prestige"],
    "image_card": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sett_0.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sett_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Sejuani",
    "surnom": "Fureur du nord",
    "classe": "Tank",
    "poste": "Jungle",
    "description": "Sejuani est l'impitoyable chef de guerre sublimée de la Griffe hivernale, l'une des tribus les plus redoutées de Freljord. La survie de son peuple est une lutte constante et désespérée contre les éléments. Pour résister aux rigueurs de l'hiver, il doit lancer des assauts contre les Noxiens, les Demaciens et les Avarosans. Sejuani mène elle-même la charge des plus dangereux de ces assauts en chevauchant son sanglier drüvask, Bristle, et en agitant son fléau de glace pure pour congeler et réduire en miettes ses ennemis. ",
    "competences": ["Fureur du nord", "Assaut arctique", "Colère de l'hiver", "Permafrost", "Prison de glace"],
    "skin": ["Sejuani", "Sejuani Défense Acérées", "Sejuani Fatale", "Sejuani Classique", "Sejuani Cavalerie Ursine", "Sejuani Chevaucheuse de Poro", "Sejuani Chasseuse de Bêtes", "Sejuani Traqueuse de l'Aube", "Sejuani Pyrotechnicienne", "Sejuani Huxtech"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte4d521b893aa5a3e/5db05ffae9d7526ab429e544/RiotX_ChampionList_sejuani.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Sejuani_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Riven",
    "surnom": "Exilée brisée",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Autrefois soldat d'un régiment de Noxus, Riven est désormais expatriée dans un pays qu'elle avait elle-même tenté d'envahir. Elle s'est naguère élevée dans la hiérarchie grâce à sa force de conviction et à son efficacité brutale : sa récompense fut de commander ses propres troupes, armée d'une épée runique légendaire. Cependant, sur le front ionien, la foi de Riven en sa terre natale a vacillé avant de rompre. Ayant tranché tous ses liens avec l'empire, elle cherche désormais sa place dans un monde blessé, alors même que, d'après les rumeurs, Noxus a retrouvé sa vigueur d'antan… ",
    "competences": ["Lame runique", "Ailes brisées", "Décharge de ki", "Bravoure", "Lame de l'exilée"],
    "skin": ["Riven", "Riven Repentie", "Riven Némésis", "Riven Lapin de Combat", "Riven du Championnat", "Riven Lame du Dragon", "Riven Arcade", "Riven du Championnat 2016", "Riven Héraut de l'Aube", "Riven Pulsefire", "Riven à l'épée Vaillant", "Riven à l'épée Vaillante Prestige", "Riven Fleur Spirituelle"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3925b81c7462e26e/5db05ffadc674266df3d5d42/RiotX_ChampionList_riven.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Riven_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Renekton",
    "surnom": "Dévoreur des sables",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Originaire du désert brûlant de Shurima, Renekton est un Transfiguré terrifiant animé par la rage. Il était autrefois le guerrier le plus respecté de l'empire de Shurima, dont il avait mené les armées vers la victoire à maintes reprises. Cependant, après la chute de l'empire, Renekton fut enseveli sous les sables. Petit à petit, alors que le monde changeait, il sombra dans la démence. Désormais libre, son unique souhait est de retrouver et de tuer son frère Nasus qui, croit-il dans sa folie, est responsable des siècles qu'il a passés dans les ténèbres. ",
    "competences": ["Règne de la colère", "Destruction des faibles", "Prédateur impitoyable", "Tranche et coupe", "Dominus"],
    "skin": ["Rennekton", "Rennekton Galactique", "Rennekton Aborigène", "Rennekton Sanguinnaire", "Rennekton des Guerres Runiques", "Rennekton de la Terre Brûlée", "Rennekton Maître Nageur", "Rennekton Préhistorique", "SKT T1 Rennekton", "Rennekton Jouet", "Rennekton Hextech", "Rennekton des Glaces Noires"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt6b5bb4d917759184/5db05ffb242f426df132f979/RiotX_ChampionList_renekton.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Renekton_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Rell",
    "surnom": "Vierge de fer",
    "classe": "Tank",
    "poste": "Support",
    "description": "Changée en arme vivante par les expériences brutales de l'académie de la Rose noire, Rell est aujourd'hui déterminée à détruire Noxus. Son enfance a été particulièrement horrible ; on lui a fait subir des choses atroces afin de perfectionner ses capacités magiques de manipulation du métal... jusqu'à ce qu'elle réussisse à s'échapper en tuant beaucoup de ses ravisseurs au passage. Désormais considérée comme une criminelle, Rell s'attaque aux soldats noxiens tout en recherchant les survivants de son ancienne « académie ». Bien qu'elle défende les faibles, elle tuera sans pitié ses anciens tortionnaires. ",
    "competences": ["Mise en pièces", "Frappe dislocatrice", "Ferromancie : Descendre en piqué", "Magnétisme", "Tempête magnétique"],
    "skin": ["Rell", "Rell Reine du Combat"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt0091d81986fe44c7/5fd43df38593bc52457eecc4/RiotX_ChampionList_Rell_v2.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Rell_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Rek'Sai",
    "surnom": "Traqueuse du Néant",
    "classe": "Combattant",
    "poste": "Jungle",
    "description": "Rek'Sai est une créature prédatrice – la plus imposante et la plus féroce de son espèce – qui se déplace sous terre pour surprendre ses proies. Sa faim insatiable a ravagé des régions entières de Shurima à l'époque où cet empire était encore florissant. Les marchands et les caravanes armées faisaient des détours de plusieurs kilomètres pour rester loin de ses terres. Dès lors que Rek'Sai vous a repéré, votre sort est scellé. Nul ne peut lui échapper ; elle est la mort qui surgit du sable.",
    "competences": ["Fureur des Xer'Sai", "Courroux de la reine/Sondeur explosif", "Enfouissement/Jaillissement", "Morsure féroce/Tunnel", "Rush du Néant"],
    "skin": ["Rek'Sai", "Rek'Sai Eternum", "Requ'Sai", "Rek'Sai des Glaces Noires"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt4affff3ef114e99b/5db05ffb347d1c6baa57be43/RiotX_ChampionList_reksai.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/RekSai_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Rammus",
    "surnom": "Tatou blindé",
    "classe": "Tank",
    "poste": "Jungle",
    "description": "Beaucoup l'idolâtrent, certains le méprisent, mais tous restent perplexes devant l'énigmatique Rammus. Protégé par une carapace cloutée, il inspire des théories de plus en plus variées sur ses origines : demi-dieu, oracle, simple bête transformée par la magie... Quelle que soit la vérité, Rammus n'est pas très bavard et ne s'arrête pour personne tandis qu'il parcourt le désert de Shurima. ",
    "competences": ["Carapace cloutée", "Démolisseur", "Boule défensive", "Provocation frénétique", "Secousses"],
    "skin": ["Rammus", "Roi Rammus", "Rammus de Chrome", "Rammus Fondu", "Rammus de Freljord", "Rammus Ninja", "Full Metal Rammus", "Rammus Gardien des Sables", "Rammus Libéro", "Rammus Hextech"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt50cccb68a58349f5/5db05ffbdc674266df3d5d48/RiotX_ChampionList_rammus.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Rammus_0.jpg",
    "eb": 1350,
    "rp": 585
},{
    "nom": "Qiyana",
    "surnom": "Impératrice des éléments",
    "classe": "Assassin",
    "poste": "Milieu",
    "description": "Dans la ville d'Ixaocan, perdue dans la jungle, Qiyana complote pour obtenir au prix du sang le siège révéré des Yun Tal. Dernière dans l'ordre de la succession, elle affronte ceux qui sont sur son chemin avec une absolue confiance en elle et une maîtrise exceptionnelle de la magie élémentaire. La terre elle-même obéit à ses ordres et Qiyana se considère comme la plus grande élémentaliste de l'histoire d'Ixaocan. À ce titre, elle pense mériter, non une ville, mais un empire. ",
    "competences": ["Privilège royal", "Courroux élémentaire/Lame d'Ixtal", "Terraforce", "Audace", "Tour de force suprême"],
    "skin": ["Qiyana", "Qiyana Boss de Combat", "True Damage Qiyana", "True Damage Qiyana Edition Prestige", "Qiyana Reine du Combat"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blta6148d8dca105d6b/5db05ff1e9effa6ba5295fa7/RiotX_ChampionList_qiyana.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Qiyana_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Pyke",
    "surnom": "Éventreur des abysses",
    "classe": "Support",
    "poste": "Support",
    "description": "Harponneur renommé des quais-abattoirs de Bilgewater, Pyke aurait dû trouver la mort dans le ventre d'une énorme créature marine… et pourtant, il est revenu. À présent, il écume les ruelles sombres de son ancien port d'attache, utilisant ses dons surnaturels pour assassiner brutalement les riches exploiteurs du peuple. Bilgewater, la ville des chasseurs de monstres, est devenue le terrain de chasse d'un monstre. ",
    "competences": ["Don des noyés", "Harponnage", "Plongée spectrale", "Ressac fantôme", "Exécution abyssale"],
    "skin": ["Pyke", "Pyke Spectre des Sables", "Pyke Lune de Sang", "Projet : Pyke", "Pyke Psychoguerrier"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt98269cb73e9fce70/5db05ff1347d1c6baa57be3d/RiotX_ChampionList_pyke.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Pyke_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Poppy",
    "surnom": "Gardienne du marteau",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Runeterra ne manque pas de braves champions, mais peu peuvent se targuer d'être aussi tenaces que Poppy. Armée du légendaire marteau d'Orlon, faisant deux fois sa taille, cette yordle déterminée cherche depuis d'innombrables années le « héros de Demacia », qui serait le dépositaire légitime de cette arme. En attendant, elle part inlassablement au combat, repoussant les ennemis du royaume à grands coups de marteau. ",
    "competences": [ "Ambassadrice de fer", "Commotion", "Présence immuable", "Charge héroïque", "Verdict de la gardienne"],
    "skin": ["Poppy", "Poppy Noxienne", "LoluPoppy", "Poppy Forgeron", "Poppy de Son", "Poppy Royale", "Poppy Royale", "Poppy Chevalier Rouge", "Poppy Gardienne des Etoiles", "Poppy Faon des Neiges", "Poppy Hextech", "Poppy Astronaute"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt0df0f7bcaf851737/5db05ff1242f426df132f973/RiotX_ChampionList_poppy.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Poppy_0.jpg",
    "eb": 450,
    "rp": 260
},{
    "nom": "Pantheon",
    "surnom": "Lance éternelle",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Autrefois l'hôte réticent de la Manifestation de la Guerre, Atreus a survécu à la mort de la puissance céleste qui l'habitait, refusant de succomber au coup qui arracha les étoiles mêmes des cieux. Avec le temps, il apprit à accepter le pouvoir de sa mortalité, et la résistance bornée qui y est associée. Atreus s'oppose désormais aux entités divines en tant que Pantheon, ressuscité parmi les mortels, et manie les armes de la Manifestation animées par sa volonté inébranlable sur le champ de bataille. ",
    "competences": ["Ardeur mortelle", "Lance astrale", "Assaut martial", "Égide impénétrable", "Météore"],
    "skin": ["Pantheon", "Pantheon Myrmidon", "Pantheon Impotoyable", "Persée Pantheon", "Full Metal Pantheon", "Pantheon Dace", "Pantheon Tueur de Dragons", "Pantheon Tueur de Zombies", "Pantheon Boulanger", "Pantheon Pulsefire"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt3f84c538bd36ef02/5db05ff17d28b76cfe43980d/RiotX_ChampionList_pantheon.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Pantheon_0.jpg",
    "eb": 3150,
    "rp": 780
},{
    "nom": "Ornn",
    "surnom": "Dieu de la forge volcanique",
    "classe": "Tank",
    "poste": "Haut",
    "description": "Ornn est le demi-dieu de la forge et de l'artisanat à Freljord. Avec comme seule compagne la solitude, il travaille dans sa forge enfouie au plus profond des cavernes de lave de l'Âtre-foyer. C'est là qu'il fond et purifie les métaux nécessaires à la création d'objets de qualité inimitable. Tandis que d'autres divinités – Volibear avant tout – s'immiscent dans les affaires des mortels, Ornn ne sort de son antre que pour remettre ces déités à leur place, avec son fidèle marteau ou les pouvoirs du volcan lui-même. ",
    "competences": ["Forgeron ambulant", "Fracture magmatique", "Fournaise", "Ruée ardente", "Appel du dieu de la forge"],
    "skin": ["Ornn", "Fulguro Ornn", "Ornn Sylvestre"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt639bacdfe8b1fd95/5db05ff1bd24496c390ae4f0/RiotX_ChampionList_ornn.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Ornn_0.jpg",
    "eb": 6300,
    "rp": 975
},{
    "nom": "Olaf",
    "surnom": "Berzerker",
    "classe": "Combattant",
    "poste": "Jungle / Haut",
    "description": "Olaf, avec sa hache et son extraordinaire force de destruction, ne souhaite que mourir au combat. Venu de l'âpre péninsule freljordienne de Lokfar, il a été choqué par une prophétie qui lui prédisait une mort paisible : dans l'esprit de son peuple, une ignoble fin de lâche. Cherchant la mort et animé par la rage, il ravage une région après l'autre, éliminant des hordes de grands guerriers et de bêtes légendaires dans sa recherche d'un ennemi capable de l'arrêter. Aujourd'hui un brutal combattant de la Griffe hivernale, il espère mourir glorieusement dans les grandes guerres à venir. ",
    "competences": ["Rage du berzerker", "Déchireuse", "Frappes vicieuses", "Frappe sauvage", "Ragnarok"],
    "skin": ["Olaf", "Olaf Déchu", "Olaf des Glaces", "Brolaf", "Olaf Pentakill", "Olaf Maraudeur", "Olaf Boucher", "SKT T1 Olaf", "Olaf Tueur de Dragons"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blte922bda45062964b/5db05ff0a6470d6ab91ce5a6/RiotX_ChampionList_olaf.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Olaf_0.jpg",
    "eb": 3150,
    "rp": 790
},{
    "nom": "Nunu et Willump",
    "surnom": "Le Garçon et son yéti",
    "classe": "Tank",
    "poste": "Jungle",
    "description": "Il était une fois un jeune garçon qui voulait prouver qu'il était un héros en tuant un monstre effrayant, mais il découvrit que la créature, un yéti solitaire, avait seulement besoin d'un ami. Rapprochés par une puissance ancienne et un amour partagé des boules de neige, Nunu et Willump courent maintenant librement dans tout Freljord en donnant vie à des aventures rêvées. Ils espèrent un jour retrouver la mère disparue de l'enfant. S'ils peuvent la sauver, peut-être seront-ils vraiment des héros, finalement… ",
    "competences": ["Appel de Freljord", "Voracité", "Boule de neige géante !", "Rafale de boules de neige", "Zéro absolu"],
    "skin": ["Nunu et Willump", "Nunu et Willump Sasquatch", "Nunu et Willump du Solstice", "Nunu et Willump Peluche", "Nunu et Bot Willump", "Nunu et Willump Blindés", "TPA et Willump", "Nunu et Willump Zombies", "Nunu et Willump Origami"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltf613746635c6d3c8/5db05ff256458c6b3fc17527/RiotX_ChampionList_nunu.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Nunu_0.jpg",
    "eb": 450,
    "rp": 260
},{
    "nom": "Nocturne",
    "surnom": "Éternel cauchemar",
    "classe": "Assassin",
    "poste": "Jungle",
    "description": "Expression démoniaque des cauchemars qui hantent tous les êtres doués d'intelligence, la chose que l'on nomme Nocturne est devenue une force primordiale du mal. D'aspect liquide, c'est une ombre sans visage, au regard glacial, armée de lames effrayantes. Après s'être affranchi du royaume des esprits, Nocturne est parvenu jusqu'au monde éveillé, pour se nourrir d'une terreur qui ne prospère que dans les véritables ténèbres. ",
    "competences": ["Lames d'ombre", "Crépuscule", "Linceul des ténèbres", "Horreur indicible", "Paranoïa"],
    "skin": ["Nocturne", "Nocturne Terreur Glaciale", "Nocturne du Néant", "Nocturne Ravageur", "Nocturne Revenant", "Nocturne Eternum", "Nocturne Spectre Maudit", "Nocturne Sylvestre", "Nocturne Hextech"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltfc73e3c7dda28d31/5db05ff2adc8656c7d24e7f0/RiotX_ChampionList_nocturne.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Nocturne_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Nautilus",
    "surnom": "Titan des profondeurs",
    "classe": "Tank",
    "poste": "Support",
    "description": "Nautilus, une légende plus ancienne encore que la première jetée de Bilgewater, est un titan en armure qui sillonne les eaux sombres au large des Îles de la Flamme Bleue. Trahi il y a bien longtemps de cela, il frappe désormais sans prévenir, se servant de son ancre gigantesque pour sauver les malheureux et couler les avares. On raconte qu'il vient chercher ceux qui oublient de payer le « tribut de Bilgewater » et qu'il les emmène avec lui au fond des mers, d'où nul ne peut s'échapper. ",
    "competences": ["Coup écrasant", "Abordage", "Colère du titan", "Répliques", "Grenade ASM"],
    "skin": ["", "Nautilus", "Nautilus des Abysses", "Nautilus Souterrain", "Astronautilus", "Nautilus Purificature", "Nautilus Brise-Monde", "Nautilus Conquérant"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltf945a57aa16cea57/5db05fe7347d1c6baa57be37/RiotX_ChampionList_nautilus.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Nautilus_0.jpg",
    "eb": 4800,
    "rp": 880
},{
    "nom": "Nasus",
    "surnom": "Gardien des sables",
    "classe": "Combattant",
    "poste": "Haut",
    "description": "Nasus est un imposant Transfiguré à tête de chacal, une figure héroïque que les peuples du désert considéraient comme un demi-dieu aux temps anciens de Shurima. Il était très intelligent ; son savoir encyclopédique et son extraordinaire sens stratégique guidèrent l'antique empire de Shurima vers la grandeur pendant des siècles. Après la chute de l'empire, il s'imposa un exil et devint peu à peu une légende. Maintenant que l'ancienne cité de Shurima renaît de ses cendres, il est de retour, déterminé à empêcher qu'elle ne chute de nouveau. ",
    "competences": ["Mangeur d'âmes", "Buveuse d'âmes", "Flétrissement", "Esprit enflammé", "Fureur des sables"],
    "skin": ["Nasus", "Nasus Galactique", "Nasus Pharaon", "Nasus Chevalier d'Effroi", "Nasus K9", "Nasus Infernal", "Archiduc Nasus", "Nasus Brise-Monde", "Nasus Gardien Lunaire", "Proto Nasus"],
    "image_card": "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/blt20a4b154b1756b56/5db05fe7a6470d6ab91ce5a0/RiotX_ChampionList_nasus.jpg",
    "image_back": "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Nasus_0.jpg",
    "eb": 1350,
    "rp": 585
}


mongo.db.leagueofstat.insert_many(champion)